<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,HasApiTokens,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phoneNo','dob','contactDetail','contactPerson','gender','socialId','login_type','type','platform','deviceToken','lat','lng','subcription_id','targetAudience','assistance_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setpasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function setnameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }
    public function service()
    {
        return $this->hasMany('App\Service', 'user_id', 'id');
    }
    public function assistance()
    {
        return $this->hasOne('App\Assistance', 'id', 'assistance_id');
    }

}
