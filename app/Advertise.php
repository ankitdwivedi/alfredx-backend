<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertise extends Model
{
    use SoftDeletes;
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'adverties';
    protected $fillable = ['user_id', 'compaignName', 'accountName','type','advertUrl','impression','click','converstions','pkspent','description','status','targetAudience'];

}
