<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketchat extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'ticketchats';
    protected $fillable = ['ticket_id', 'sender_id', 'reciever_id', 'message', 'ticketFrom'];

    public function getLastRepliedUser() {
	return $this->belongsTo('App\User', 'sender_id', 'id');//  hasMany('App\User', 'sender_id', 'id');
    }

}
