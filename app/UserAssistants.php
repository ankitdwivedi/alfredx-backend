<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAssistants extends Model
{
    protected $fillable = ['userId', 'assistantId', 'isCurrent'];
    
    public function assistant_detail() {
	return $this->hasOne('App\Assistance', 'id', 'assistantId');//  hasMany('App\User', 'sender_id', 'id');
    }

}
