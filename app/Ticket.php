<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tickets';
    protected $fillable = ['user_id', 'ticketFrom', 'ticketName', 'description', 'priority', 'status'];

    public function getAllTickets() {
        $user = Auth::user();
        $loginId = $user->id ?? '';
        $result = Ticket::where('user_id', $loginId)->get();
        return Ticket::where('user_id', $loginId)->get();
    }

    public function ticket_chat() {
        return $this->hasMany('App\Ticketchat', 'ticket_id', 'id');
    }
    
}
