<?php

namespace App\Http\Controllers\Api;

use App\Reminder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RemindersController extends Controller {

    /**
     * Selecting all records in active.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
	$reminders = Reminder::where("user_id", $request->user_id)->get();
	return response()->json($reminders, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
	try {
	    $reminder = Reminder::create($request->all());
	    if ($reminder) {
		return response()->json(["message" => "Successfully created"], 200);
	    }
	} catch (Exception $e) {
	    return response()->json(["message" => "Please enter valid data"], 401);
	}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function show(Reminder $reminder) {
	//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function edit(Reminder $reminder) {
	//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reminder $reminder) {
	$reminder->update($request->all());
	if ($reminder)
	    return response()->json(["message" => "Successfully updated"], 200);
	return response()->json(["message" => "Please enter valid data"], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reminder  $reminder
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
	$reminder = Reminder::destroy($id);
	if ($reminder)
	    return response()->json(["message" => "Successfully deleted"], 200);
	return response()->json(["message" => "Reminder does not exist"], 401);
    }

    public function updateStatus(Request $request, Reminder $reminder) {
	try {
	    
	    $response = array('response' => '', 'success' => false);
	    
	    $rules = array(
            'id' => 'required',
            'status' => 'required'
        );
	    $validator = Validator::make($request->all(), $rules);
	    if ($validator->fails()) {
		$response['response'] = $validator->messages();
	    } else {
		$reminder = Reminder::where('id', $request->id)->update(['status' => $request->status]);
		if ($reminder)
		return response()->json(["message" => "Successfully updated"], 200);
		
		return response()->json(["message" => "Please enter valid data"], 401);
	    }	
	    return $response;
	} catch (\PHPUnit\Framework\Exception $ex) {
	    return response()->json(['message' => 'Something went wrong.'], 400);
	}
    }

}
