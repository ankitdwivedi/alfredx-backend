<?php

namespace App\Http\Controllers\Api;

use App\Ticket;
use App\Ticketchat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {
            $tickets = (new Ticket)->getAllTickets();
            if ($tickets) {
                return response()->json($tickets, 200);
            }
        } catch (\PHPUnit\Framework\Exception $ex) {
            return response()->json(['message' => 'Something went wrong.'], 400);
        }
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $ticket = Ticket::create($request->all());
            if ($ticket) {
                return response()->json(["message" => "Successfully created"], 200);
            }
        } catch (Exception $e) {
            return response()->json(["message" => "Please enter valid data"], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket) {
        return response()->json($ticket, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket) {
        $ticket->update($request->all());
        if ($ticket) {
            return response()->json(["message" => "Successfully created"], 200);
        }
        return response()->json(["message" => "Please enter valid data"], 401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket) {
        $ticket->delete();
        return response()->json(["message" => "Successfully created"], 200);
    }

    /**
     * Send message for ticketing
     *   
     * @return \Illuminate\Http\Response
     */
    public function postMsgTicket(Request $request) {
        $ticketchat = Ticketchat::create($request->all());
        if ($ticketchat)
            return response()->json(["message" => "Successfully created"], 200);
        return response()->json(["message" => "Please enter valid data"], 401);
    }

    /**
     * get ticketing message
     *   
     * @return \Illuminate\Http\Response
     */
    public function getMsgTicket(Request $request, $id) {
        $ticketchats = Ticketchat::where('ticket_id', $id)
                        ->offset($request->offset)->limit(10)->orderby('created_at', 'desc')->get();
        return response()->json($ticketchats, 200);
    }
    
    public function reopenTicket(Request $request) {
	
        $ticketUpdate = Ticket::where('id', $request->ticketId)->update(['status' => 'reopen']);
        if ($ticketUpdate) {
            return response()->json(["message" => "Ticket Reopened Successfully!"], 200);
        }
        return response()->json(["message" => "Error In Ticket Open"], 401);
    }

}
