<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Validator;
use Carbon\Carbon;
use App\User;
use App\UserAssistants;
use Storage;
use Hash;

class AuthController extends Controller {

    use SendsPasswordResetEmails;

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request) {
	$validate = $this->validations($request, 'signup');
	if ($validate["error"]) {
	    return response()->json([
			'message' => json_decode($validate["errors"])->email[0],
			    ], 401);
	}
	$user = new User([
	    'name' => $request->name,
	    'email' => $request->email,
	    'password' => $request->password
	]);
	$user->save();
	return response()->json([
		    'message' => 'Successfully created user!',
		    'user' => $user
			], 200);
    }

    public function validations($request, $type) {
	$errors = [];
	$error = false;
	if ($type == "signup") {

	    $validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'email' => 'required|string|email|unique:users',
			'password' => 'required|string'
	    ]);

	    if ($validator->fails()) {
		$error = true;
		$errors = $validator->errors();
	    }
	}
	return ["error" => $error, "errors" => $errors];
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request) {
	$request->validate([
	    'email' => 'required|string|email',
	    'password' => 'required|string',
	    'remember_me' => 'boolean'
	]);
	$credentials = request(['email', 'password']);
	if (!Auth::attempt($credentials))
	    return response()->json([
			'message' => 'Email or Password incorrect'
			    ], 401);

	$user = $request->user();
	$tokenResult = $user->createToken('Personal Access Token');
	$token = $tokenResult->token;
	if ($request->remember_me)
	    $token->expires_at = Carbon::now()->addWeeks(1);
	$token->save();

	return response()->json([
		    'access_token' => $tokenResult->accessToken,
		    'token_type' => 'Bearer',
		    'user' => $user,
		    'user_assistant' => $user->assistance,
		    'expires_at' => Carbon::parse(
			    $tokenResult->token->expires_at
		    )->toDateTimeString()
			], 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request) {
	$request->user()->token()->revoke();
	return response()->json([
		    'message' => 'Successfully logged out'
	]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function profileUpdate(Request $request, $id) {
	if (isset($_FILES['profile_image']) && $_FILES['profile_image'] != '') {

	    $image = $request->file('profile_image');
	    $imagename = time() . '.' . $image->getClientOriginalExtension();
	    $destinationPath = public_path('/images/users');
	    $image->move($destinationPath, $imagename);

	    $usersUpdate = User::where('id', $id)->update(['profile_image' => $imagename]);
	}
	if (isset($request->assistance_id) && $request->assistance_id != '') {
	   
	    $userAssistants = UserAssistants::Where('userId', $id)->where('assistantId',$request->assistance_id)->get();
	   
	    if($userAssistants->isEmpty()){
		$usersUpdate = UserAssistants::where('userId', $id)->update(['isCurrent' => '0']);
		$userAssistant = new UserAssistants([
		    'userId' =>  $id,
		    'assistantId' => $request->assistance_id,
		    'isCurrent' => '1'
		]);
		$userAssistant->save();
	    }else{
		$usersUpdate = UserAssistants::where('userId', $id)->update(['isCurrent' => '0']);
		$usersUpdate = UserAssistants::where('userId', $id)->where('assistantId',$request->assistance_id)->update(['isCurrent' => '1']);
	    }
	}
	
	$data = $request->except(['profile_image','assistance_id']);
	$usersUpdate = User::where('id', $id)->update($data);


	if ($usersUpdate) {
	    $updatedData = User::where('id', $id)->get();
	    return response()->json(["message" => "Successfully updated", "updatedData" => $updatedData->toArray()], 200);
	}
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request) {
	$this->validateEmail($request);
	$response = $this->broker()->sendResetLink(
		$request->only('email')
	);
	if ($response == "passwords.sent") {
	    return response()->json([
			'message' => 'Reset password link has been sent to your mail'
			    ], 200);
	}
	return response()->json([
		    'message' => 'Email does not exists'
			], 401);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function resetPassword(Request $request, $id) {
	$user = User::where('id', $id)->update(["password" => bcrypt($request->password)]);
	if ($user)
	    return response()->json(["message" => "Successfully updated"], 200);
	return response()->json(["message" => "Enter valid password"], 401);
    }

    /**
     * social sign up.
     *
     * @param  [string] name
     * @param  [string] socialId
     * @return [string] 
     */
    public function socialLogin(Request $request) {
	if (isset($request->socialId) && !empty($request->socialId)) {
	    $user = User::where('socialId', $request->socialId)->first();
	    if ($user) {
		return $this->generateToken($user);
	    } else {
		$res = $this->socialSignup($request, 'socialId', $request->socialId);
		if ($res) {
		    $data = User::find($res->id);
		    return $this->generateToken($data);
		    //generate token and response to the user 
		}
	    }
	} else {
	    return response()->json([
			'message' => 'invalid request'
			    ], 401);
	}
    }

    public function socialSignup($request, $field, $sid) {
	$name = isset($request->name) ? $request->name : null;
	$login_type = isset($request->login_type) ? $request->login_type : 1;
	$email = $sid . "@flynaut.com";
	return User::create(['name' => $name, "email" => $email, "password" => $sid, $field => $sid, "login_type" => $login_type]);
    }

    public function generateToken($user) {
	$tokenResult = $user->createToken('Personal Access Token');
	$token = $tokenResult->token;
	$token->expires_at = Carbon::now()->addWeeks(1);
	$token->save();
	return response()->json([
		    'access_token' => $tokenResult->accessToken,
		    'token_type' => 'Bearer',
		    'user' => $user,
		    'expires_at' => Carbon::parse(
			    $tokenResult->token->expires_at
		    )->toDateTimeString()
			], 200);
    }

    public function UpdateMyProfile(Request $request) {
	$id = Auth::user()->id ?? '';
	if ($id) {

	    if (isset($_FILES['profile_image']) && $_FILES['profile_image'] != '') {

		$image = $request->file('profile_image');
		$imagename = time() . '.' . $image->getClientOriginalExtension();
		$destinationPath = public_path('/images/users');
		$image->move($destinationPath, $imagename);

		$usersUpdate = User::where('id', $id)->update(['name' => $request->name, 'dob' => $request->dob, 'phoneNo' => $request->phoneNo, 'profile_image' => $imagename]);


		if ($usersUpdate) {
		    $updatedData = User::where('id', $id)->get();
		    return response()->json(["message" => "Successfully updated", "updatedData" => $updatedData->toArray()], 200);
		}
	    } else {

		$usersUpdate = User::where('id', $id)->update(['name' => $request->name, 'dob' => $request->dob, 'phoneNo' => $request->phoneNo]);

		if ($usersUpdate) {
		    $updatedData = User::where('id', $id)->get();
		    return response()->json(["message" => "Successfully updated", "updatedData" => $updatedData->toArray()], 200);
		}
	    }

	    return response()->json(["message" => "Please enter valid data"], 401);
	} else {
	    return response()->json(["message" => "Invalid user access"], 401);
	}
    }

    public function changePassword(Request $request) {

	$requestData = $request->all();
	$id = Auth::user()->id ?? '';
	$user = User::find($id);
	$current_password = $user->password;

	if (Hash::check($requestData['old_password'], $current_password)) {
	    $user = User::where('id', $id)->update(["password" => bcrypt($requestData['new_password'])]);
	    return response()->json(["message" => "Successfully updated"], 200);
	} else {
	    return response()->json(["message" => "Enter valid old password"], 401);
	}
    }

}
