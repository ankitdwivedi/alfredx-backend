<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Service;
use App\Reminder;
use App\UserAssistants;
class ServiceController extends Controller
{
    /**
    * Create new service
    * @param subject,question,assistance_id 
    * @return [string] success response 
    */
    public function createService(Request $request)
    {
    	try{
    		// $data['subject'] = $request->subject;
    		// $data['question'] = $request->question;
    		// $data['assistance_id'] = $request->assistance_id;
    		$service = Service::create($request->all());
    		if($service){
    			return response()->json(["message"=>"You have been requested a new service"],200);
    		}
    	} catch (Exception $e) {            	
    		return response()->json(["message"=>"Please enter valid data"],401);
    	}
    }

     /**
    * Get all request
    * @param subject,question,assistance_id 
    * @return [string] success response 
    */
     public function getService(Request $request)
     {
     	try{
            $services=[];
            switch ($request->status) {
                case 'all':
                $services = Service::where('user_id',$request->user_id)->get();
                break;
                case 'recent':
                $services = Service::where('user_id',$request->user_id)->orderby('updated_at','desc')->get();
                break;
                case 'open':
                $services = Service::where('user_id',$request->user_id)->where('status','open')->orderby('updated_at','desc')->get();
                break;
                case 'closed':
                $services = Service::where('user_id',$request->user_id)->where('status','closed')->orderby('updated_at','desc')->get();
                break;
            }
            return response()->json($services,200);
        }catch(exception $e){
         return response()->json(["message"=>"Internal server error"],401);
     }
 }

    /**
    * View service count.
    * @param user_id 
    * @return [string] success response 
    */
    public function serviceDashboard(Request $request)
    {
        try{
            $services = Service::where('user_id',$request->user_id)->get();
            $collection = collect($services);
            $services = [
                "all"=>$collection->count(),
                "recent"=>Service::where('user_id',$request->user_id)->orderby('updated_at','desc')->count(),
                "open"=>$collection->where('status', 'open')->count(),
                "closed"=>$collection->where('status', 'closed')->count()
            ];
            $reminders=[];
            if(isset($request->from) && isset($request->to)){
                $reminders = Reminder::where("user_id",$request->user_id)->whereBetween('reminderDate', [$request->from, $request->to])->get();
            }else if(isset($request->from)){
                $reminders = Reminder::where("user_id",$request->user_id)->where('reminderDate','>=', $request->from)->get();
            }else{
                $reminders = Reminder::where("user_id",$request->user_id)->get();
            }
            return response()->json([
                "services"=>$services,
                "reminders"=>$reminders
            ],200); 
        } catch (Exception $e) {                
            return response()->json(["message"=>"internal server error"],500);
        }
    }

    
    /**
    * View service count.
    * @param user_id, assistance_id 
    * @return [string] success response 
    */
    public function getAssistanceService(Request $request,$assistance_id)
    {
        try{
            $services = Service::where('user_id',$request->user_id)
            ->where('assistance_id',$assistance_id)
            ->orderby('updated_at','desc')
            ->get();
            return response()->json($services,200);
        } catch (Exception $e) {                
            return response()->json(["message"=>"internal server error"],500);
        }   
    }

    /**
    * update service status.
    * @param serive_id, status 
    * @return [string] success response 
    */

    public function updateServiceStatus(Request $request,$id)
    {
        $services = Service::find($id)->update($request->all());
        if($services)
            return response()->json(Service::find($id),200);
        return response()->json(["message"=>"Please enter valid data"],401);
    
    }
    
    public function getAllAssitant(Request $request) {
	try{
	    $services = UserAssistants::with('assistant_detail')->where('userId',Auth::user()->id)->get();
//	    dd($services[0]->getAssistantDetails);assistant_details
	    return response()->json($services,200);
        } catch (Exception $e) {                
            return response()->json(["message"=>"internal server error"],500);
        }   
    }
  


}
