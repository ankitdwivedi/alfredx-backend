<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Assistance;

class ExternalController extends Controller {

    /**
     * Get assistance records
     *
     * @return array of assistanc 
     */
    public function getAssistance(Request $request) {
        $assistances = Assistance::all();
        return response()->json($assistances, 200);
    }

    public function updateAssistant(Request $request) {
        $data = $request->all();
        $assistant = Assistance::find($data['assistant_id']);
        if($assistant) {
            $assistant->name = $data['name'];
	    $assistant->avatar = $data['avatar'];
            $assistant->save();
            return response()->json($assistant, 200);
        } else {
            return response()->json(array('status' => 'failure', 'message' => 'No assistant found'), 200);
        }
    }

}
