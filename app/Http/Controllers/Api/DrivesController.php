<?php

namespace App\Http\Controllers\Api;

use App\Drive;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use DB;

class DrivesController extends Controller
{
    /**
     * Display a listing of the drives for a users.
     * @param    user_id
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $drives = Drive::whereIn('service_id', function($query) use ($request)
        {
          $query->select('id')
          ->from('services')
          ->where('services.user_id',$request->user_id);
      })
        ->get();
        return response()->json($drives,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function show(Drive $drive)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function edit(Drive $drive)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Drive $drive)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Drive  $drive
     * @return \Illuminate\Http\Response
     */
    public function destroy(Drive $drive)
    {
        //
    }
}
