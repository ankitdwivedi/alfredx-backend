<?php

namespace App\Http\Controllers\Api;

use App\Advertise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdvertisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $reminder = Advertise::create($request->all());
            if($reminder){
                return response()->json(["message"=>"Successfully created"],200);
            }
        } catch (Exception $e) {                
            return response()->json(["message"=>"Please enter valid data"],401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function show(Advertise $advertise)
    {
        return response()->json($advertise,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertise $advertise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertise $advertise)
    {
        $advertise->update($request->all());
        if($advertise)
            return response()->json(["message"=>"Successfully created"],200);
        return response()->json(["message"=>"Please enter valid data"],401);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertise $advertise)
    {
        $advertise->delete();
        if($advertise)
            return response()->json(["message"=>"Successfully created"],200);
        return response()->json(["message"=>"Advertise does not exist"],401);
    }
}
