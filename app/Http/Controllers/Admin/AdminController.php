<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
//use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $paginator = User::where('type', 'admin')->paginate(10);
        return view('admin.admin_users.index', compact('paginator'));

        // return view('admin.client.index',$paginator);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
//        dd($request->all());
//        dd(Input::get('email'));
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/admin_users')
                            ->withErrors($validator)
                            ->withInput(Input::except('password'));
        } else {
            // store
            $data = $request->except(['_token', 'retype_password']);
            $query = User::create($data);
            Session::flash('success', 'Successfully created user!');
            return Redirect::to('admin/admin_users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $user = User::where('id', $id)->with(['assistance'])->get();
        $viewParams = [
            'admin_users' => $user
        ];
        return view('admin.admin_users.view', $viewParams);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        dd($request->all());
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
	User::destroy($id);
	Session::flash('success', 'User Deleted successfully!');
	return response()->json(['status' => 'success']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function change_password(Request $request) {
        //
//        dd($request->all());
//        dd(Input::get('email'));
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/admin_users')
                            ->withErrors($validator)
                            ->withInput(Input::except('password'));
        } else {
            // store
            $data = $request->all();
            $user = User::find($data['id']);
            $user->password = $data['password'];
            $user->save();

            Session::flash('success', 'Password successfully updated!');
            return Redirect::to('admin/admin_users');
        }
    }

}
