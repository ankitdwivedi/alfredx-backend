<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
//use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class CmsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
	$cmspages = Cms::all();
	return view('admin.cms.view', compact('cmspages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Cms $cmspage) {
	return view('admin.cms.form', compact('cmspage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
	//
//        dd($request->all());
//        dd(Input::get('email'));
	// validate
	// read more on validation at http://laravel.com/docs/validation
        $rules = array(
	    'title' => 'required',
	    'content' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/cms/create')
                            ->withErrors($validator)
                            ->withInput();
        } else {
	// store
	$data = $request->except(['_token']);
	$query = Cms::create($data);
	Session::flash('success', 'Successfully created cms!');
	return Redirect::to('admin/cms');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function show(Cms $cms) {
	//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
	$cmspage = Cms::findOrFail($id);


	return view('admin.cms.form', compact('cmspage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, Cms $cms)
//    {
//        //
//    }

    public function update(Request $request, $id) {
	
	$rules = array(
	    'title' => 'required',
	    'content' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/cms/'.$id.'/edit')
                            ->withErrors($validator)
                            ->withInput();
        } else {
	$page = Cms::findOrFail($id);

	$page->fill($request->only('title', 'content'))->save();

	Session::flash('success', 'Successfully Updated Cms!');
	return Redirect::to('admin/cms');
	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cms  $cms
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

	Cms::destroy($id);

	Session::flash('success', 'Record Successfully Deleted!');
	return response()->json(['status' => 'success']);
    }

}
