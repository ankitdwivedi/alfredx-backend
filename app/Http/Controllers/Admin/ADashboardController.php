<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Service;
use App\Assistance;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use DB;

class ADashboardController extends Controller
{

    public function index(Request $request) 
    {
        $totalusers = User::where('type','user')->count(); 
        $date = Carbon::today()->subDays(7);
        $last7days = User::where('created_at', '>=', $date)->count();

        $assistants =  Assistance::with(['service'=>function($query){
            $query->get();
        }])->get();

        /*Assistants max servics*/
        $services =  User::where('type','user')->with(['service'=>function($query){
            $query->get();
        }])->get();

        /*Top5list*/
        $servicelists = DB::table('services')->select('user_id',DB::raw('count("user_id") as count'))->groupby('user_id')->orderby('count','desc')->get();
        $collection = collect($servicelists);
        $plucked = $collection->pluck('user_id');
        $top5lists = User::whereIn('id',$plucked)->limit(5)->get(); 

        $viewParams = [
            'totalUsers'=>$totalusers,
            'last7days'=>$last7days,
            'top5lists'=>$top5lists,
            'assistants'=>$assistants
        ];
        return view('admin.adashboard', $viewParams);
    }
}
