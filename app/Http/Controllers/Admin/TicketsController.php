<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;
use App\Ticketchat;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
//use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class TicketsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
	$userTickets = Ticket::Where('ticketFrom', 'user')->get();
	$all_view = 1;
	$pending_view = 0;
	return view('admin.tickets.clientview', compact('userTickets', 'pending_view', 'all_view'));
    }

    public function pendingTickets() {
	$userTickets = Ticket::Where('ticketFrom', 'user')->Where('status', 'open')->get();
	$all_view = 0;
	$pending_view = 1;
	return view('admin.tickets.clientview', compact('userTickets', 'pending_view', 'all_view'));
    }

    public function storeMessageclient(Request $request) {

	$rules = array(
	    'message' => 'required'
	);
	$validator = Validator::make(Input::all(), $rules);

	// process the login
	if ($validator->fails()) {
	    return Redirect::to('admin/tickets/clientTicketview/' . $request->ticket_id)
			    ->withErrors($validator)
			    ->withInput();
	} else {
	    // store
	    $data = $request->except(['_token']);
	    $query = Ticketchat::create($data);
	    return Redirect::to('admin/tickets/clientTicketview/' . $request->ticket_id);
	}
    }

    public function clientTicketview($id) {
	$userTicketChat = Ticketchat::Where('ticket_id', $id)->get();
	$userTickets = Ticket::Where('id', $id)->first();
	$ticketId = $id;
	return view('admin.tickets.clientTicketview', compact('userTicketChat', 'userTickets', 'ticketId'));
    }

    public function closeClientTicket(Request $request) {
	$ticketdetails = Ticket::findOrFail($request->ticket_id);
	$requestData['status'] = $request->status;
	$ticketdetails->update($requestData);
	return Redirect::to('admin/tickets/clients/');
    }

    public function advertiser() {
	$advertiserTickets = Ticket::Where('ticketFrom', 'advert')->get();
	$all_view = 1;
	$pending_view = 0;
	return view('admin.tickets.advertiserview', compact('advertiserTickets', 'pending_view', 'all_view'));
    }

    public function advertiserTicketview($id) {
	echo "ada1d"; die;
	$userTicketChat = Ticketchat::Where('ticket_id', $id)->get();
	$userTickets = Ticket::Where('id', $id)->first();
	$ticketId = $id;
	return view('admin.tickets.advertiseTicketview', compact('userTicketChat', 'userTickets', 'ticketId'));
    }

    public function closeAdvertiseTicket(Request $request) {

	$ticketdetails = Ticket::findOrFail($request->ticket_id);
	$requestData['status'] = $request->status;
	$ticketdetails->update($requestData);
	return Redirect::to('admin/tickets/advertiser/');
    }

    public function storeMessageadvertise(Request $request) {
	$rules = array(
	    'message' => 'required'
	);
	$validator = Validator::make(Input::all(), $rules);

	// process the login
	if ($validator->fails()) {
	    return Redirect::to('admin/tickets/advertiser/' . $request->ticket_id)
			    ->withErrors($validator)
			    ->withInput();
	} else {
	    // store
	    $data = $request->except(['_token']);
	    $query = Ticketchat::create($data);
	    return Redirect::to('admin/tickets/advertiser/' . $request->ticket_id);
	}
    }
    
    public function pendingTicketsAdvertiser() {
	$advertiserTickets = Ticket::Where('ticketFrom', 'advert')->Where('status', 'open')->get();
	$all_view = 0;
	$pending_view = 1;
	return view('admin.tickets.advertiserview', compact('advertiserTickets', 'pending_view', 'all_view'));
    }

}
