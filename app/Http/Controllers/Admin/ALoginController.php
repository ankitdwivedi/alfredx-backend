<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class ALoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
	$this->middleware('guest')->except('logout');
    }

    public function index() {
	return view('admin.alogin');
    }

    public function attemptLogin(Request $request) {
	$request->validate([
	    'email' => 'required|string|email|max:255',
	    'password' => 'required|string|max:30',
	]);
	$data['email'] = request('email');
	$data['password'] = request('password');
	if (Auth::attempt($data)) {
	    if (Auth::user()->type == "admin") {
		return redirect()->route('adashboard');
	    } else {
		$this->logout();
		return redirect()->back()->with('alert-danger', '<strong>Invalid!</strong> Email or Password')->withInput();
	    }
	} else {
	    return redirect()->back()->with('alert-danger', '<strong>Invalid!</strong> Email or Password')->withInput();
	}
    }

    public function logout() {
	Auth::logout();
	return redirect('/admin/login');
    }

}
