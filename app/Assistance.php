<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assistance extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'assistances';
    protected $fillable = ['name', 'avatar'];

    public function service()
    {
    	return $this->hasMany('App\Service', 'assistance_id', 'id');
    }
}
