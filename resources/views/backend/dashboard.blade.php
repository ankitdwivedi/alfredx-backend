@extends('backend.layouts.master')

@section('page_css')
input[type="checkbox"]{
  display: none;
}
  .btn span.glyphicon {         
  visibility: hidden;     
}
.btn.active span.glyphicon {        
  visibility: visible;      
}


.fs { font-weight: bold; }

#myupdateTarget .modal-sm{
  width: 400px !important;
}
@endsection


@section('content')
<div class="container">
  <div class="row justify-content-md-center top-ads-row">
    <div class="col-md-4">
      <div class="col-md-12 text-center"><img  class="icons" src="{{ asset('images/Ads@2x.png') }}" alt=""><span class="ads-number" >12</span></div>
      <div class="col-md-12 text-muted text-center ad-details">Total Ads</div>
    </div>

    <div class="col-md-3">
      <div class="col-md-12 text-center"><img  class="icons" src="{{ asset('images/Reach.png') }}" alt=""><span class="ads-number" >11240</span></div>
      <div class="col-md-12 text-muted text-center ad-details">Total impressions</div>
    </div>

    <div class="col-md-3">
      <div class="col-md-12 text-center"><img  class="icons" src="{{ asset('images/money.png') }}" alt=""><span class="ads-number" >$230</span></div>
      <div class="col-md-12 text-muted text-center ad-details">Total money spent</div>
    </div>
  </div>
  <div class="row no-gutters table-top-header">
    <ul class="mr-auto left-list">
      <li><span class="label">Account</span> <span class="label-field">Company Name</span></li>
      <li><span class="label">Date</span><span class="label-field">Nov 1 2018</span></li>
      <li><span class="label">Status</span><span class="label-field">Active</span></li>
    </ul>
    <ul class="ml-auto right-list">
      <li><button class="btn a-red-btn" data-toggle="modal" data-target="#myupdateTarget"><span><img src="{{ asset('/images/Grupo.png') }}" alt=""></span>&nbsp;&nbsp;&nbsp;New Ad Campaign</button></li>
    </ul>
  </div>

  <div class="a-box">
      <table class="a-table">
        
        <thead>
          <div class="row">
            <div class="container-fluid">
              <div class="col-md-12 table-top-row">
                <div class="row">
                  <div class=" col-md-2 table-title" style="">Ads</div>
                  <div class="col-md-2 col-sm-6 table-search-col ml-auto"><span><img src="{{ asset('/images/search.png') }}" alt=""></span><input type="text" name="" placeholder="Search here" class=""><div>
                </div>
              </div>
            </div>
          </div>
          <tr>
            <th> <input type="checkbox" class="a-checkbox">Campaign Name</th>
            <th>Account Name</th>
            <th>Type</th>
            <th>Impressions</th>
            <th>Clicks</th>
            <th>Conversions</th>
            <th>Package/Spent</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> <input type="checkbox" class="a-checkbox">Campaign Name</td>
            <td>Account Name</td>
            <td>Type</td>
            <td>Impressions</td>
            <td>Clicks</td>
            <td>Conversions</td>
            <td>Package/Spent</td>
          </tr>
          <tr>
            <td> <input type="checkbox" class="a-checkbox">Campaign Name</td>
            <td>Account Name</td>
            <td>Type</td>
            <td>Impressions</td>
            <td>Clicks</td>
            <td>Conversions</td>
            <td>Package/Spent</td>
          </tr>
        </tbody>
      </table>
  </div>
</div>




<div class="modal fade " id="myupdateTarget">
  <div class="modal-dialog modal-sm  modal-dialog-centered">

    <div class="modal-content fs">

      <!-- Modal Header -->
       <h2 class='text-center pt-5'>Target Ads (Default)</h2>
    <div class="row mt-5  mb-5 justify-content-center">
                     <div class="btn-group col-md-10" data-toggle="buttons">
                          <label class="btn btn-danger active">
                                 <input type="checkbox" autocomplete="off" checked>
                                <span class="glyphicon glyphicon-ok"></span>
                          </label> &nbsp&nbspTarget Ads to user near my location 
                    </div>
                      <div class="btn-group col-md-10" data-toggle="buttons">
                            <label class="btn btn-danger active">
                              <input type="checkbox" autocomplete="off" checked>
                              <span class="glyphicon glyphicon-ok"></span>
                          </label> &nbsp&nbspTarget Ads to specific gender
                    </div>
                  <div class=" col-sm-10  mb-5 offset-sm-2 gender-radio-group">
                      <input id="radio-1" class="a-radio-custom " name="radio-group" type="radio" >
                      <label for="radio-1" class="radio-custom-label">Male</label>   <br>         
                      <input id="radio-1" class="a-radio-custom" name="radio-group" type="radio" >
                      <label for="radio-1" class="radio-custom-label">Female</label> <br>
                      <input id="radio-1" class="a-radio-custom" name="radio-group" type="radio" >
                      <label for="radio-1" class="radio-custom-label">Others</label>
                  </div>
              
               <div class="modal-footer">
        <button type="submit" class="big-ok-button btn a-btn a-btn-login ">Update</button>
         <button type="submit" class="big-ok-button btn a-btn " data-dismiss="modal" aria-label="Close">Cancel</button>
       </div>
         </div> 
      
        </div>
      
      </div>

      <!-- Modal footer -->

      


</div>


@endsection