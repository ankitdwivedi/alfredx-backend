@extends('backend.layouts.master')

@section('page_css')
body{
  overflow:hidden;
}

.cimg{
  height:80px;
  weidth:80px;
  padding-top:20px;
}

<!-- * {
    box-sizing: border-box;
}
 -->
.zoom {
    padding: 1px;
    background-color: white;
   box-shadow: 0 16px 24px 2px rgba(0, 0, 0, .14), 0 6px 30px 5px rgba(0, 0, 0, .12), 0 8px 10px -5px rgba(0, 0, 0, .2);
    border-radius:15px;
    transition: transform .2s;
    min-height: 60vh;
    width:10px;
    position:relative
    margin: 0 auto;
   }

.zoom:hover {
    -ms-transform: scale(1.2); 
    -webkit-transform: scale(1.2); 
    transform: scale(1.2);
    cursor: pointer;
    z-index: 99999;
    
}
.zoom.active{
    -ms-transform: scale(1.2); 
    -webkit-transform: scale(1.2); 
    transform: scale(1.2);
  
    z-index: 4;   
}
.s-card{
  margin-left: 20%;
  margin-right: 20%;

}
  
.moremargin{
  margin-top:70px;
} 



h1{
  color:red;
}
@endsection


@section('content')
<div class="container">
      <div class="row text-center justify-content-md-center mt-5 ">

        <div class="col-md-3 mx-1">
           <div class="row">
             <div class="col-md-12 zoom ">
                <section>
                   <img src="{{ asset('images/Group 5@2x.png') }}" class="cimg" alt="">
                   <h1 class="m-5">Individual</h1>
                   <p class="s-card text-muted">sed upperspiciates unde omnis iste</p>
                   <h1 class="m-5">$0.99</h1>
                </section>
             </div>
           </div>
         <button type="submit" class="big-ok-button btn a-btn a-btn-login mt-5 ">Select Plan</button>
        </div>

        <div class="col-md-3 mx-1" >
           <div class="row ">
             <div class="col-md-12 zoom active">
                <section >
                  <img src="{{ asset('images/best package (2) section@2x.png') }}" class="cimg" alt="">
                  <h1 class="m-5">Team</h1>
                  <p class="text-muted s-card">Ipsum is simply dummy text of the printing and sed upperspiciates unde omnis iste Ipsum is simply dummy text of the printing and</p>
                  <h1 class="m-5">$15.99</h1>
                </section>
        </div>
      </div>
     <h3 class="moremargin">Current Plan</h3> 
    </div>
      
        <div class="col-md-3 mx-1 " >
           <div class="row ">
             <div class="col-md-12 zoom">
                <section >
                   <img src="{{ asset('images/Group 4@2x.png') }}" class="cimg" alt="">
                   <h1 class="m-5">Enterprice</h1>
                   <p class="s-card text-muted">sed upperspiciates unde omnis iste</p>
                   <h1 class="m-5">$0599</h1>
                </section>
              </div>
          </div>
         <button type="submit" class="big-ok-button btn a-btn a-btn-login mt-5 ">Select Plan</button>
        </div>

      </div>
    </div>
      <!-- Modal footer -->


@endsection