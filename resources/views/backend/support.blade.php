@extends('backend.layouts.master')

@section('page_css')

@endsection


@section('content')
<div class="container">
  <div class="row justify-content-md-center top-ads-row">
    <div class="col-md-4">
      <div class="col-md-12 text-center"><img  class="icons" src="{{ asset('images/support@2x.png') }}" alt=""><span class="ads-number" >0</span></div>
      <div class="col-md-12 text-muted text-center ad-details">Total Ads</div>
    </div>

    
  </div>
  <div class="row no-gutters table-top-header">
    <ul class="mr-auto left-list">
      <li><span class="label">Account</span> <span class="label-field">Company Name</span></li>
      <li><span class="label">Date</span><span class="label-field">Nov 1 2018</span></li>
      <li><span class="label">Status</span><span class="label-field">Active</span></li>
    </ul>
    <ul class="ml-auto right-list">
      <li><button class="btn a-red-btn" data-toggle="modal" data-target="#myupdateTarget"><span><img src="{{ asset('/images/Grupo.png') }}" alt=""></span>&nbsp;&nbsp;&nbsp;Create New Ticket</button></li>
    </ul>
  </div>

  <div class="a-box">
      <table class="a-table ">
        
        <thead>
          <div class="row">
            <div class="container-fluid">
              <div class="col-md-12 table-top-row">
                <div class="row">
                  <div class=" col-md-2 table-title" style="">Ads</div>
                  <div class="col-md-2 col-sm-6 table-search-col ml-auto"><span><img src="{{ asset('/images/search.png') }}" alt=""></span><input type="text" name="" placeholder="Search here" class=""><div>
                </div>
              </div>
            </div>
          </div>
          <tr >
            <th> <input type="checkbox" class="a-checkbox text-muted"><span class=" text-muted">Ticket Title</span></th>
            <th class="text-muted">Ticket Discreption</th>
            <th class="text-muted">Priority</th>
            <th class="text-muted">Date</th>
            <th class="text-muted"><i class="fa fa-arrow-down" aria-hidden="true"></i>Status</th>
            
          </tr>
        </thead>
        

      </table>
      <div class="row mt-5 mb-5">
        <div class="col-md-12 mb-5 mt-5">
           <h3 class="mt-5 mb-5 text-muted text-center">We couldn't find any tickets. For any query create a new Ticket</h3>
        </div>
      </div>
      
  </div>
</div>





      


</div>


@endsection