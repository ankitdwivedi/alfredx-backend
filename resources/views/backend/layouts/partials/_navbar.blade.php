<nav class="navbar navbar-expand-lg">
  <img class="brand-logo" src="{{ asset('/images/alfredx.png') }}" alt="">
  <ul class="navbar-menu-list ">
      <li class="nav-item ">
        <a class="nav-link active" href="#">Campaigns</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Subscription</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link " href="#">Support</a>
      </li>
  </ul>
  <ul class="user-nav ml-auto">
    <li>
      <a href="#">{{ isset(auth()->user()->name) ? auth()->user()->name : 'Guest' }}</a>
    </li>
    <li class="user-avatar">
      <a href="#" id="user-icon" onclick="toggleVisiblity()"><img class="user-avatar-img" src="{{ asset('/images/User@2xwhite.png') }}" alt=""></a>
      <div id="login-option" class="login-option">
        <ul class="list-group">
          <li><a href="#" data-toggle="modal" data-target="#editProfile">Edit Profile</a></li>
          <li><a href="#" >Target Ads (Default)</a></li>
          <li><a href="#" data-toggle="modal" data-target="#changePassword">Change Password</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="{{ route('logout') }}">Logout</a></li>
        </ul>
      </div>
    </li>
    <li>
      <div class="menu-toggler">
        <div class="btn active">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </li>
  </ul>
</nav>

<div class="row message-row">
  <div class="col-md-6 offset-md-3 col-sm-10 offset-sm-1 col-12 flash-message">@include('backend.layouts.partials._flashMessage')</div>
</div>

<!-- Edit Profile Modal -->
<div class="modal fade editProfile" id="editProfile">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header col-md-12 text-center">
        <h4 class="modal-title text-center">Edit Profile</h4>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
    <form method="POST" action="{{ route('updateProfile') }}">
      {{ csrf_field() }}
        <div class="form-group">
          <input type="text"  name="email" class="form-control a-text" value="{{ isset(auth()->user()->email) ? auth()->user->email : '' }}">
        </div>
        <div class="form-group">
          <input type="text" name="name" class="form-control a-text" placeholder="Full Name" 
          value="{{ isset(auth()->user()->name) ? auth()->user()->name : '' }}">
        </div>
        <div class="form-group">
          <input type="text" name="contactPerson" class="form-control a-text" placeholder="Contact Person"value="{{ isset(auth()->user()->contactPerson) ? auth()->user()->contactPerson : '' }}">
        </div>
        <div class="form-group">
          <input type="text" name="phoneNo" class="form-control a-text" placeholder="Phone number" 
          value="{{ isset(auth()->user()->phoneNo) ? auth()->user()->phoneNo : ''}}">
        </div>
        
      </div>

      <!-- Modal footer -->
      <div class="modal-footer text-left">
        <div class="form-group mr-auto">
          <button type="submit" class="btn a-btn a-btn-login">Update</button>
        </div>
        <a href="#" class="a-link " data-dismiss="modal">Cancel</a>
      </div>
  </form>
    </div>
  </div>
</div>
<!-- Change Password Modal -->
<div class="modal fade changePassword" id="changePassword">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header col-md-12 text-center">
        <h4 class="modal-title text-center">Change Password</h4>
      </div>

      <form action="{{ route('changePassword') }}" method="post">
        <!-- Modal body -->
        {!! csrf_field() !!}
        <div class="modal-body">
          <div class="form-group">
            <input type="text" autocomplete="off" name="current-password"class="form-control a-text" placeholder="Current Password">
          </div>
          <div class="form-group">
            <input type="password" name="new-password" class="form-control a-text" placeholder="New Password">
          </div>
          <div class="form-group">
            <input type="password"  name="confirm-password" class="form-control a-text" placeholder="Confirm Password">
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer text-left">
          <div class="form-group mr-auto">
            <button type="submit" class="btn a-btn a-btn-login">Update</button>
          </div>
          <a href="#" class="a-link" data-dismiss="modal">Cancel</a>
        </div>

      </form>
    </div>
  </div>
</div>


@section('page_script')
  $(document).on('click', '#user-icon', function(){
    loginOption = $('#login-option');
    loginOption.toggleClass('active');
  });
@append