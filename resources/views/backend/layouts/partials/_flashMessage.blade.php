  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
        <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">
           <button type="button" class="close" data-dismiss="alert">&times;</button>
          </button>
            {!! Session::get('alert-' . $msg) !!}				  	
        </div>
      @endif
  @endforeach
