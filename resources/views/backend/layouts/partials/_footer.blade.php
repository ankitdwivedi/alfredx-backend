  
  
  <script src="{{ asset('/js/app.js') }}"></script>

  <script>
    @yield('page_script')
  </script>

  <script>
    $(document).ready(function(){
      @yield('page_ready_script')
    });
  </script>

</body>

</html>