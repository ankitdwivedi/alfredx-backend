@include('backend.layouts.partials._header')

@include('backend.layouts.partials._navbar')


@yield('content')

@include('backend.layouts.partials._footer')