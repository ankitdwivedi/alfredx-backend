@extends('backend.layouts.master')



@section('content')





	@include('backend.layouts.partials._subMenu')
	<div class="a-sub-menu">
		<ul class="list-inline row">
			<li><a href="#" class="option-btn selected"><span>Campaign</span></a></li>
			<li><a href="#" class="option-btn"><span>Ad</span></a></li>
			<li><a href="#" class="option-btn"><span>Target Audience</span></a></li>
			<li><a href="#" class="option-btn"><span>Publish</span></a></li>
		</ul>
	</div>


	<div class="container">
	    <h2 class='text-center pt-5'>AD</h2>
		<p class='text-center text-muted	'>Uplod Your ad copy</p>

		<div class="a-box al-card" style="width:100%;">
		  <div class="card-body mb-5">
		  	<div class="row">
		  		<div class="col-md-4 offset-md-4 alert alert-success text-center mx-auto">  
					<span> Woot! Your Creatives are uploaded now.</span>
				</div>
		  	</div>
			<br>
			<br>
		    <h5 class="card-title"></h5>
		   <div class="row">
			   	<div class="card-ad-img col-md-6 offset-md-3">
				   	<button type="button" class="close" aria-label="Close">
				   		<span aria-hidden="true">&times;</span>
				   	</button>
				   	<img src="{{ asset('/images/previewimage@2x.png') }}" alt='image not found'>
				 </div> 
		   </div> 
		  </div>
		</div>

		<div class="bottom-menu row"> 
			<ul class="left-nav-list list-inline">
				<li>
					<span>Powered by <img style="height: 30px; width: 80px"src="{{ asset('/images/LogoAlfaN-2016T-2-1024x386@2x.png') }}"></span>
				</li>
			</ul>
			<ul class="right-nav-list list-inline">
				<li><a type="" href="#" class="a-link">Back</a></li>
				<li><button type="" class="btn a-btn a-red-btn ">Preview</button></li>
			 	<li><button type="" class="btn a-btn a-red-btn ">Next</button></li>
			</ul>
		</div>

	</div>


<script>
	// Add active class to the current button (highlight it)
	var header = document.getElementById("sub-menu");
	var btns = header.getElementsByClassName("btn");
	for (var i = 0; i < btns.length; i++) {
	  btns[i].addEventListener("click", function() {
	    var current = document.getElementsByClassName("active");
	    current[0].className = current[0].className.replace(" active", "");
	    this.className += " active";
	  });
	}
</script>
@endsection