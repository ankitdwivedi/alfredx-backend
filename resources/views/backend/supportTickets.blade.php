@extends('backend.layouts.master')

@section('page_css')
.modelwidth{
  height:100;
  width:380px;
}
.small-btn{
  width: 140px;
  height: 42px;
  font-weight: 500;
  font-size: 18px;
  
}

@endsection


@section('content')
<div class="container">
  <div class="row justify-content-md-center top-ads-row">
    <div class="col-md-4">
      <div class="col-md-12 text-center"><img  class="icons" src="{{ asset('images/support@2x.png') }}" alt=""><span class="ads-number" >0</span></div>
      <div class="col-md-12 text-muted text-center ad-details">Total Ads</div>
    </div>

    
  </div>
  <div class="row no-gutters table-top-header">
    <ul class="mr-auto left-list">
      <li><span class="label">Account</span> <span class="label-field">Company Name</span></li>
      <li><span class="label">Date</span><span class="label-field">Nov 1 2018</span></li>
      <li><span class="label">Status</span><span class="label-field">Active</span></li>
    </ul>
    <ul class="ml-auto right-list">
      <li><button class="btn a-red-btn" data-toggle="modal" data-target="#ticketmodel"><span><img src="{{ asset('/images/Grupo.png') }}" alt=""></span>&nbsp;&nbsp;&nbsp;Create New Ticket</button></li>
    </ul>
  </div>

  <div class="a-box">
      <table class="a-table ">
        
        <thead>
          <div class="row">
            <div class="container-fluid">
              <div class="col-md-12 table-top-row">
                <div class="row">
                  <div class=" col-md-2 table-title" style="">Ads</div>
                  <div class="col-md-2 col-sm-6 table-search-col ml-auto"><span><img src="{{ asset('/images/search.png') }}" alt=""></span><input type="text" name="" placeholder="Search here" class=""><div>
                </div>
              </div>
            </div>
          </div>
          <tr >
            <th> <input type="checkbox" class="a-checkbox text-muted"><span class=" text-muted">Ticket Title</span></th>
            <th class="text-muted">Ticket Discreption</th>
            <th class="text-muted">Priority</th>
            <th class="text-muted">Date</th>
            <th class="text-muted"><i class="fa fa-arrow-down" aria-hidden="true"></i>Status</th>
            
          </tr>
        </thead>
        

     
      
                        
               <tbody >
                    <tr>
                      <td><b>Need help uploading image</b></td>
                      <td class="text-muted">Lorem Ipsum is simply dummy text of..</td>
                      <td class="text-muted">High</td>
                      <td class=" text-muted">11/02/2018</td>
                      <td class="text-success">Open</td>

                    </tr>


                    <tr>
                        <td><b>Need help uploading image</b></td>
                      <td class="text-muted">Lorem Ipsum is simply dummy text of..</td>
                      <td class="text-muted">High</td>
                      <td class=" text-muted">11/02/2018</td>
                      <td class="text-success">Open</td>

                    </tr>


                    <tr>
                       <td><b>Need help uploading image</b></td>
                      <td class="text-muted">Lorem Ipsum is simply dummy text of..</td>
                      <td class="text-muted">High</td>
                      <td class=" text-muted">11/02/2018</td>
                      <td class="text-success">Open</td>

                    </tr>

                    <tr class="bg-light">
                       <td><b>Need help uploading image</b></td>
                      <td class="text-muted">Lorem Ipsum is simply dummy text of..</td>
                      <td class="text-muted">High</td>
                      <td class=" text-muted">11/02/2018</td>
                      <td class="text-danger">Closed</td>

                    </tr>

                    <tr >
                       <td><b>Need help uploading image</b></td>
                      <td class="text-muted">Lorem Ipsum is simply dummy text of..</td>
                      <td class="text-muted">High</td>
                      <td class=" text-muted">11/02/2018</td>
                      <td class="text-danger ">Closed</td>

                    </tr>
               </tbody>
         </table>
      </div>
 </div>
  <!-- Modal -->
  <div class="modal fade" id="ticketmodel" role="dialog">
    <div class="modal-dialog modelwidth">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">New Ticket</h4>
         
        </div>
        <div class="modal-body">
          <form>
          <div class="row">       
             <div class="col-md-12">
            <input type="text" placeholder="Ticket title" name="" class="form-control a-text ">
             <input type="text" placeholder="Priority" name="" class="form-control a-text  mt-5 mb-5">
               <textarea name="comment" placeholder="Description" class="form-control  a-text "></textarea>
             
              </div>


          </div>
          </form>
        </div>
        <div class="modal-footer">

               <div class="modal-footer col-md-12">
                <div class="row">
                  <div class="col-md-6 col-6">
                     <button type="submit" class=" btn small-btn a-btn-login ">Create</button>
                  </div>
                  <div class="col-md-6 col-6">
                    <button type="submit"  class=" btn small-btn bg-white" style="font-size: 16px; color: #323232; font-weight: 600" data-dismiss="modal" aria-label="Close">Cancel</button>
                  
                  </div>

                </div>
       
       </div>
          
        </div>
      </div>
      
    </div>
  </div>
  

      
      



@endsection