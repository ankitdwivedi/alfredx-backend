@extends('backend.layouts.master')

@section('page_css')
.write-something{
  position: relative;
}
.write-something button{
  position: absolute;
  right:0;
  top:50%;
  transform: translateY(-50%);
}


@endsection


@section('content')
<div class="container">
  <div class="row justify-content-md-center top-ads-row">
    <div class="col-md-4">
      <div class="col-md-12 text-center"><img  class="icons" src="{{ asset('images/support@2x.png') }}" alt=""><span class="ads-number" >5</span></div>
      <div class="col-md-12 text-muted text-center ad-details">Total Ads</div>
    </div>

    
  </div>
  <div class="row no-gutters table-top-header">
    <div class="col-md-12"><h3><b>Back</b></h3></div>
    <ul class="mr-auto left-list">
      <li><span class="label">Account</span> <span class="label-field">Company Name</span></li>
      <li><span class="label">Date</span><span class="label-field">Nov 1 2018</span></li>
      <li><span class="label">Status</span><span class="label-field">Active</span></li>
    </ul>
    <ul class="ml-auto right-list">
      <li><button class="btn a-red-btn" data-toggle="modal" data-target="#myupdateTarget"><span><img src="{{ asset('/images/Grupo.png') }}" alt=""></span>&nbsp;&nbsp;&nbsp;Create New Ticket</button></li>
    </ul>
  </div>

  <div class="a-box">
      <table class="a-table ">
        
        <thead>
          <div class="row">
            <div class="container-fluid">
              <div class="col-md-12 table-top-row">
                <div class="row">
                  <div class=" col-md-6 table-title" style="">Need help uploading image</div>
                  <div class="col-md-1">High</div>
                  <div class="col-md-3">11/02/2018</div>
                  <div class="col-md-1 text-success">Open</div>
                  <div class="col-md-12 mt-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>
                 
                </div>
              </div>
            </div>
          </div>
          
        </thead>
      
        
        
             
 
  <div class="input-group py-5">
  <input type="text" class="form-control py-4" placeholder="Write here" aria-label="Recipient's username">
  <div class="input-group-append">
    <button class="input-group-text bg-danger text-light" ><span><img src="{{asset('')}}" alt=""></span></button>
  </div>
</div>
  
       
        <tbody>    
         
       
      <tr >
         
        <td class=" text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</td>
       

      </tr>

      <tr >
         
        <td class=" text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</td>
       

      </tr>
      <tr >
         
        <td class=" text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</td>
       

      </tr>

     
    </tbody>
     </table>
        </div>
      </div>
      
  </div>
</div>





      


</div>


@endsection