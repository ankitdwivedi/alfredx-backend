@extends('backend.layouts.master')

@section('page_css')
input[type="checkbox"]{
  display: none;
}
  .btn span.glyphicon {    			
	visibility: hidden;			
}
.btn.active span.glyphicon {				
	visibility: visible;			
}


@endsection


@section('content')
	<div class="a-sub-menu">
		<ul class="list-inline row">
			<li><a href="{{ route('campaign.create') }}" id="menu-campaign"        class="option-btn"><span>Campaign</span></a></li>
			<li><a href="{{ route('campaignAd.create') }}" id="menu-ad"              class="option-btn "><span>Ad</span></a></li>
			<li><a href="#" id="menu-target-audience" class="option-btn selected"><span>Target Audience</span></a></li>
			<li><a href="#" id="menu-publish"         class="option-btn"><span>Publish</span></a></li>
		</ul>
	</div>
	
	<div class="container ">
	    <h2 class='text-center pt-5'>Target Audience</h2>
		<p class='text-center text-muted	'>Customise the audience that you want to cater your ad.</p>

		<div class="a-box al-card" style="width:100%;">
		  <div class="card-body mt-5 mb-5">
		  
		   <div class="row mt-5 mb-5 justify-content-center">
                     <div class="btn-group col-md-7 offset-md-2" data-toggle="buttons">
                          <label class="btn btn-danger active">
                                 <input type="checkbox" autocomplete="off" checked>
                                <span class="glyphicon glyphicon-ok"></span>
                          </label> &nbsp &nbspTarget Ads to user near my location 
                    </div>
                      <div class="btn-group col-md-7 offset-md-2" data-toggle="buttons">
                            <label class="btn btn-danger active">
                              <input type="checkbox" autocomplete="off" checked>
                              <span class="glyphicon glyphicon-ok"></span>
                          </label> &nbsp &nbspTarget Ads to specific gender
                    </div>
                  <div class=" col-md-7 mb-5 offset-md-3 gender-radio-group">
                      <input id="radio-1" class="a-radio-custom " name="radio-group" type="radio" >
            					<label for="radio-1" class="radio-custom-label">Male</label>   <br>         
                      <input id="radio-1" class="a-radio-custom" name="radio-group" type="radio" >
            					<label for="radio-1" class="radio-custom-label">Female</label> <br>
                      <input id="radio-1" class="a-radio-custom" name="radio-group" type="radio" >
            					<label for="radio-1" class="radio-custom-label">Others</label>
                  </div>
              
				 </div> 
		   </div> 
		  </div>
		</div>

		<div class="bottom-menu row"> 
			<ul class="left-nav-list list-inline">
				<li>
					<span>Powered by <img style="height: 30px; width: 80px"src="{{ asset('/images/LogoAlfaN-2016T-2-1024x386@2x.png') }}"></span>
				</li>
			</ul>
			<ul class="right-nav-list list-inline">
				<li><a type="" href="#" class="a-link">Back</a></li>
			 	<li><button type="" class="btn a-btn a-red-btn ">Next</button></li>
			</ul>
		</div>

	</div>


<script>

</script>
@endsection