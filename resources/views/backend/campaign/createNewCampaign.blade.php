@extends('backend.layouts.master')



@section('content')





	@include('backend.layouts.partials._subMenu')

	<div class="container">
	    <h2 class='text-center pt-5'>Create your campaign</h2>
		<p class='text-center text-mute'>Give your campaign a unique name</p>

		<div class="a-box al-card" style="width:100%;">
		  <div class="card-body mb-5">
		    <div class="form-group col-md-10">
		    	<label class="a-label">Campaign Name</label>
		    	<input type="text" class="form-control a-text" name="">
		    </div>
		    <div class="form-group col-md-12">
		    	<label class="a-label">Type of ad</label>
		    	<div class="row">
		    		<div class="col-md-5">
		    			<div class="row">
		    				<div class="col-sm-2 checkbox-div">
		    					<input id="radio-1" class="a-radio-custom" name="radio-group" type="radio" >
            					<label for="radio-1" class="radio-custom-label"></label>
		    				</div>
		    				<div class="col-sm-8 px-0"><img style="width: 100%"src="{{ asset('/images/11-Google.png') }}"></div>
		    			</div>
		    		</div>

		    		<div class="col-md-5">
		    			<div class="row">
		    				<div class="col-sm-2 checkbox-div">
		    					<input id="radio-1" class="a-radio-custom" name="radio-group" type="radio" >
            					<label for="radio-1" class="radio-custom-label"></label>
		    				</div>
		    				<div class="col-sm-8 px-0"><img style="width: 100%"src="{{ asset('/images/17-Your Reminders.png') }}"></div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		  </div>
		</div>

		<div class="bottom-menu row"> 
			<ul class="left-nav-list list-inline">
				<li>
					<span>Powered by <img style="height: 30px; width: 80px"src="{{ asset('/images/LogoAlfaN-2016T-2-1024x386@2x.png') }}"></span>
				</li>
			</ul>
			<ul class="right-nav-list list-inline">
				<li><a type="" class="a-link">Back</a></li>
			 	<li><button type="" class="btn a-btn a-red-btn ">Next</button></li>
			</ul>
		</div>

	</div>

@endsection