@extends('backend.layouts.master')



@section('content')

	<div class="a-sub-menu">
	<ul class="list-inline row">
		<li><a href="#" id="menu-campaign"        class="option-btn"><span>Campaign</span></a></li>
		<li><a href="#" id="menu-ad"              class="option-btn selected"><span>Ad</span></a></li>
		<li><a href="#" id="menu-target-audience" class="option-btn"><span>Target Audience</span></a></li>
		<li><a href="#" id="menu-publish"         class="option-btn"><span>Publish</span></a></li>
	</ul>
</div>


	<div class="container" >
	    <h2 class='text-center pt-5'>AD</h2>
		<p class='text-center text-muted	'>Upload Your ad copy</p>
<form action="" method="post"> <!--form start -->
		<div class="a-box al-card" style="width:100%;">
		  <div class="card-body mb-5">
		  	<div class="row">
		  		<div class="col-md-4 offset-md-4 alert alert-success text-center mx-auto">  
					<span> Woot! Your Creatives are uploaded now.</span>
				</div>
		  	</div>
			<br>
			<br>
		    <h5 class="card-title"></h5>
		   <div class="row">
			   	<div class="card-ad-img col-md-6 offset-md-3">
				   	<button type="button" class="close" aria-label="Close">
				   		<span aria-hidden="true">&times;</span>
				   	</button>
				   	<img src="{{ asset('/images/previewimage@2x.png') }}" alt='image not found'>
				 </div> 
		   </div> 
		  </div>
		</div>

		<div class="bottom-menu row"> 
			<ul class="left-nav-list list-inline">
				<li>
					<span>Powered by <img style="height: 30px; width: 80px"src="{{ asset('/images/LogoAlfaN-2016T-2-1024x386@2x.png') }}"></span>
				</li>
			</ul>
			<ul class="right-nav-list list-inline">
				<li><a type="" class="a-link">Back</a></li>
				<li><button type="" class="btn a-btn a-red-btn" data-toggle="modal" data-target="#editPreviewAd">Preview</button></li>
			 	<li><button type="" class="btn a-btn a-red-btn ">Next</button></li>
			</ul>
		</div>
</form>
	</div>



	<!-- Edit Preview Modal -->
<div class="modal fade editPreviewAd" id="editPreviewAd">
  <div class="modal-dialog modal-sm">

    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header col-md-12 text-center">
        <button type="button" class="close text-right" data-dismiss="modal">&times;</button>
        <br>
        <h2 class="modal-title" id="previewAd">Preview</h2>
      </div>

      <!-- Modal body -->

      <div class="modal-body" id="border-design">
    <form method="POST" action="{{ route('updateProfile') }}">
      {{ csrf_field() }}

		
			  <h4 class="modal-title text-left">Question:</h4>
      		<div style="margin-bottom: 30px;" class="card-ad-img col-md-12">
				<textarea class="a-box text-muted" placeholder="Type here your Question"></textarea>
			</div> 

        	<div style="margin-bottom:30px" class="card-ad-img col-md-12">
				<img src="{{ asset('/images/previewimage@2x.png') }}" alt='image not found'>
			</div> 

     
        	<div class="form-group col-md-12">
          		<button type="submit" style="width: 100%"class="btn a-btn a-btn-login btn-block">Start Conversation</button>
		    </div>
  		</form>
      </div>

      <!-- Modal footer -->

       <div class="modal-footer">
				<button type="submit" class="big-ok-button btn a-btn a-btn-login ">OK</button>
       </div>


</div>

@endsection