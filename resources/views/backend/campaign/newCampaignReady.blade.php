@extends('backend.layouts.master')



@section('content')
	<div class="a-sub-menu">
		<ul class="list-inline row">
			<li><a href="{{ route('campaign.create') }}" id="menu-campaign"        class="option-btn"><span>Campaign</span></a></li>
			<li><a href="{{ route('campaignAd.create') }}" id="menu-ad"              class="option-btn "><span>Ad</span></a></li>
			<li><a href="#" id="menu-target-audience" class="option-btn"><span>Target Audience</span></a></li>
			<li><a href="#" id="menu-publish"         class="option-btn selected"><span>Publish</span></a></li>
		</ul>
	</div>
	
	<div class="container">
	    <h2 class='text-center pt-5'>Publish</h2>
		<p class='text-center text-muted	'>Upload you ad copy</p>

		<div class="a-box al-card" style="width:100%;">
		  <div class="card-body mb-5">
		  	<div class="row">
		  		<div class="col-md-4  offset-3 mx-auto">  
					<h1 class="display-3"> READY TO GO !</h1> 
				</div>
		  	</div>
			<br>
			
		    
		   <div class="row offset-3 text-center mx-auto mb-5 mt-5">
			    <div class="col-md-12  ">
			    	  <button type="button" class="btn mb-5 a-btnlarge">GO LIVE</button><br>
			    	 <span>Back</span>
			    </div>
		   </div> 
		  </div>
		</div>

		<div class="bottom-menu row"> 
			<ul class="left-nav-list list-inline">
				<li>
					<span>Powered by <img style="height: 30px; width: 80px"src="{{ asset('/images/LogoAlfaN-2016T-2-1024x386@2x.png') }}"></span>
				</li>
			</ul>
			<ul class="right-nav-list list-inline">
				<li><a type="" href="#" class="a-link">Back</a></li>
			 	<li><button type="" class="btn a-btn a-red-btn ">Next</button></li>
			</ul>
		</div>

	</div>
@endsection