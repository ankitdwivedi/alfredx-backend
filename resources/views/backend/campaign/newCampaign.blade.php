@extends('backend.layouts.master')



@section('content')
	<div class="a-sub-menu">
		<ul class="list-inline row">
			<li><a href="{{ route('campaign') }}" id="menu-campaign"        class="option-btn"><span>Campaign</span></a></li>
			<li><a href="{{ route('campaignAd.create') }}" id="menu-ad"              class="option-btn selected"><span>Ad</span></a></li>
			<li><a href="#" id="menu-target-audience" class="option-btn"><span>Target Audience</span></a></li>
			<li><a href="#" id="menu-publish"         class="option-btn"><span>Publish</span></a></li>
		</ul>
	</div>
	
	<div class="container">
	    <h2 class="text-center display-2">AD</h2>
		<p class="text-center text-muted	">Uplod Your ad copy</p>

		<div class="a-box al-card" style="width:100%;">
		  <div class="card-body mb-5">
		  	<div class="row">
		  		<div class="col-md-4 offset-md-4 alert alert-success text-center mx-auto">  
					<span> Woot! Your Creatives are uploaded now.</span>
				</div>
		  	</div>
			<br>
			<br>
		    <h5 class="card-title"></h5>
		   <div class="row">
			   	<div class="card-ad-img col-md-6 offset-md-3">
				   	<button type="button" class="close" aria-label="Close">
				   		<span aria-hidden="true">&times;</span>
				   	</button>
				   	<img src="{{ asset('/images/previewimage@2x.png') }}" alt='image not found'>
				 </div> 
		   </div> 
		  </div>
		</div>

		<div class="bottom-menu row"> 
			<ul class="left-nav-list list-inline">
				<li>
					<span>Powered by <img style="height: 30px; width: 80px"src="{{ asset('/images/LogoAlfaN-2016T-2-1024x386@2x.png') }}"></span>
				</li>
			</ul>
			<ul class="right-nav-list list-inline">
				<li><a type="" href="#" class="a-link">Back</a></li>
			 	<li><button type="" class="btn a-btn a-red-btn ">Next</button></li>
			</ul>
		</div>

	</div>


<script>

</script>
@endsection