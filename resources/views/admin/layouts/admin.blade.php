<!DOCTYPE html>
<html lang="en">
<head>
	<title>Alfredx Admin</title>
	<!-- META INFORMATION -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- STYLESHEETS -->
	<link href="{{asset('/plugins/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('/css/css/app.css')}}" rel="stylesheet" type="text/css">
	<?php if(isset($styles)) echo $styles; ?>
	<script src="{{asset('/js/app.js')}}"></script>
	<script src="{{asset('/js/actions/action.js')}}"></script>
	<?php if(isset($scripts)) echo $scripts; ?>
	<!-- SCRIPTS -->
</head>
<body class="fixed-left">
	<div id="wrapper">
		<!-- Top Bar Start -->		
		<!-- Top Bar End -->
		<!-- ========== Left Sidebar Start ========== -->
		<div class="left side-menu">
			<div class="sidebar-inner slimscrollleft">
				
				<div class="user-details">
					<div class="">
						<img src="{{asset('/images/Logo@2x.png')}}" alt="" class="sidelogo">
					</div>
				</div>

				<!--- Divider -->
				<div id="sidebar-menu">
					
					<div class="clearfix"></div>
					<ul>
						<li><a href="{{url('admin/dashboard')}}" class="waves-effect waves-light {{ Request::segment(2)=='dashboard' ? 'active':'' }}"><img src="{{asset('/images/amenu2.png')}}" alt="" class="thumb-md"><span>Dashboard</span></a>
						</li>
						<li><a href="{{url('admin/clients')}}" class="waves-effect waves-light {{ Request::segment(2)=='clients' ? 'active':'' }}"><img src="{{asset('/images/amenu3.png')}}" alt="" class="thumb-md"><span> Manage Clients</span></a>
						</li>
						<li>
							<a href="{{url('admin/assistant')}}" class="waves-effect waves-light {{ Request::segment(2)=='assistant' ? 'active':'' }}"><img src="{{asset('/images/amenu1.png')}}" alt="" class="thumb-md"><span> Manage Assistant</span></a>
						</li>

						<li>
							<a href="{{url('admin.manageAdvert')}}" class="waves-effect waves-light {{ Request::segment(2)=='manageAdvert' ? 'active':'' }}"><img src="{{asset('/images/amenu4.png')}}" alt="" class="thumb-md"><span> Manage Advisertiser</span></a>
						</li>

						<li>
							<a href="{{url('admin/admin_users')}}" class="waves-effect waves-light {{ Request::segment(2)=='manageAdmin' ? 'active':'' }}"><img src="{{asset('/images/amenu5.png')}}" alt="" class="thumb-md"><span> Manage Admin</span></a>
						</li>

						<li>
							<a href="{{url('admin/cms')}}" class="waves-effect waves-light {{ Request::segment(2)=='contentManagement' ? 'active':'' }}"><img src="{{asset('/images/amenu6.png')}}" alt="" class="thumb-md"><span> Content Managment</span></a>
						</li>
						<li>
							<a href="#" class="waves-effect waves-light {{ Request::segment(2)=='ticketSupport' ? 'active':'' }}"><img src="{{asset('/images/amenu7.png')}}" alt="" class="thumb-md"><span> Ticket Support</span></a>
							<ul>
							   <li>
							       <a href="{{url('admin/tickets/clients')}}" class="{{ Request::segment(2)=='clients' ? 'active':'' }}"><span>Client Tickets</span></a> </li>
							   <li>
							       <a href="{{url('admin/tickets/advertiser')}}" class="{{ Request::segment(2)=='advertiser' ? 'active':'' }}"><span>Advertiser Tickets</span></a> </li>
							</ul>
						</li>

					</ul>
					<hr />
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- Left Sidebar End --> 
		<!-- ============================================================== -->
		<!-- Start right Content here -->
		<!-- ============================================================== -->                      
		<div class="content-page">
			<div class="container cus-container">
				<div class="row">
					<div class="col-sm-6 left">
						<h1>{{ Request::segment(2)=='dashboard' ? 'Dashboard':'' }}</h1>
						<h1>{{ Request::segment(2)=='clients' ? 'Manage Clients':'' }}</h1>
						<h1>{{ Request::segment(2)=='assistant' ? 'Manage Assistant':'' }}</h1>
					</div>
					<div class="col-sm-6">
						<div style="float:right;">
							<img src="{{asset('/images/anotify.png')}}" alt="" class="thumb-md">
							<span style="color:#000;">Admin Name</span>
							<a href="{{route('alogout')}}" title="Logout"><img src="{{asset('/images/auserprofile.png')}}" alt="" class="thumb-md"></a>
						</div>
					</div>

				</div>
			</div>
			@yield('content')
		</div>
		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
	</div>
	@if(session('warning'))
	<div class="alert alert-warning alert-float" role="alert">
		{{ session('warning') }}
		<button type="button" class="close" data-dismiss="alert">
			<span>&times;</span>
		</button>
	</div>
	@elseif(session('success'))
	<div class="alert alert-success alert-float" role="alert">
		{{ session('success') }}
		<button type="button" class="close" data-dismiss="alert">
			<span>&times;</span>
		</button>
	</div>
	@elseif(session('failure'))
	<div class="alert alert-danger alert-float" role="alert">
		{{ session('failure') }}
		<button type="button" class="close" data-dismiss="alert">
			<span>&times;</span>
		</button>
	</div>
	@endif

	<script>
		var resizefunc = [];
	</script>
	<!-- jQuery  -->
	<script src="{{asset('/js/detect.js')}}"></script>
	<script src="{{asset('/js/fastclick.js')}}"></script>
	<script src="{{asset('/js/jquery.slimscroll.js')}}"></script>
	<script src="{{asset('/js/jquery.blockUI.js')}}"></script>
	<script src="{{asset('/js/waves.js')}}"></script>
	<script src="{{asset('/js/wow.min.js')}}"></script>
	<script src="{{asset('/js/jquery.nicescroll.js')}}"></script>
	<script src="{{asset('/js/jquery.scrollTo.min.js')}}"></script>
	<script src="{{asset('/js/jquery.app.js')}}"></script>
	<!-- moment js  -->
	<script src="{{asset('/plugins/sweetalert/dist/sweetalert.min.js')}}"></script>
	<script src="{{asset('/plugins/moment/moment.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			setTimeout(function() {
				$('.alert-float').fadeOut('fast');
			}, 4000);
		});
	</script>>
	
	@yield('css')
	@yield('js')
	

</body>
</html>
