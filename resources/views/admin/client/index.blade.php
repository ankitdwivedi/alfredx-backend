@extends('admin.layouts.admin')

@section('content')
<!-- Start content -->
<div class="content">
	<div class="container">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">

						<div class="row">
							<div class="create_user col-md-1" style="font-weight: 800;font-size: 20px;">
								<p>Clients</p>
							</div>
							<div class="search-button col-md-8">
								<input type="text" name="search" value="" placeholder="search by name,phone number" />
							</div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th>Sr No.</th>
									<th>Name</th>
									<th>Phone Number</th>
									<th>Registration Date</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($paginator as $client)
								<tr>									
									<td class="text-center">{{$client->id}}</td>
									<td class="">{{$client->name}}</td>
									<td class="">{{$client->phoneNo}}</td>
									<td class="">{{$client->dob}}</td>
									<td class="text-center"><a class="a-btn-link red-col">Block</a>&nbsp;&nbsp;<a href="{{url('admin/clients',[$client->id])}}" class="a-btn-link">view</a></td>
								</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
					<div style="float:right">{{$paginator}}</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->
		
	</div> <!-- container -->
</div> <!-- content -->

<!-- 
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">

		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Video</h5>
			</div>
			<form method="POST" action="{{url('updateVideo')}}" class="form-horizontal" enctype="multipart/form-data" >

				<div class="form-group form-inline">
					<label for="category" class="inline-lab" >Category Name:</label>
					<select class="form-control" name="item_id" id="maincat">
						<option value="">Select Category</option>
					</select>
				</div>
				<div class="form-group form-inline">
					<label for="category" class="inline-lab" >SubCategory Name:</label>
					<select class="form-control" name="subcat" id="subcat">
					</select>
				</div>
				<div class="form-group form-inline">
					<label for="video-title" class="inline-lab">Video Title:</label>
					<input type="text" class="form-control" id="video-title" name="title">
					<span class="control-fileupload" style="margin:12px;display: inline-block;vertical-align:bottom;">
						<label for="file" class="file-label">choose Video :</label>
						<input type="file" id="file" name=file>
					</span>
				</div>
				<input type="text" id="vidid" name="vid_id" hidden>
				<input type="text" id="v_url" name="v_url" hidden>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">UPDATE</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>

			</form>			

		</div>

	</div>
</div> -->

<script type="text/javascript">
	
	$('#exampleModal3').on('show.bs.modal', function (event) {
		$('#maincat').html('<option value="">Select Category</option>');
		$('#subcat').html('<option value="">Select SubCategory</option>');
		var $options = $("#maincat2 > option").clone();
		$('#maincat').append($options);
		var button = $(event.relatedTarget); 
		var data = button.data('whatever');
		console.log(data); 
		var split = data.split(",");
		var modal = $(this);
		
		console.log(split[5]);

		if(split[0] == ''){
			$("#maincat option[value='"+split[6]+"']").attr("selected", true);		
		}else{
			$("#maincat option[value='"+split[5]+"']").attr("selected", true);
			$.ajax({
				type: "GET",
				url: "getSubCat",
				data: {id:split[5]},
				cache: false,
				success: function(result){
					if(result.length != 0){	
						$.each(result, function(key, value) {   
							$('#subcat').append($("<option></option>").attr('value',value.id).text(value.name)); 
						});
						$("#subcat option[value='"+split[6]+"']").attr("selected", true);
					}else{
						$('#subcat').html('<option value="">Select SubCategory</option>');
					}
				}
			})
		}
		$("#video-title").val(split[2]);
		$(".file-label").text(split[3]);
		$("#vidid").val(split[4]);
		$("#v_url").val(split[3]);

	})
	
</script>
@stop