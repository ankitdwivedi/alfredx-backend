@extends('admin.layouts.admin')

@section('content')
<!-- Start content -->
<div class="content">
	<div class="container">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="create_user col-md-6" style="font-weight: 800;font-size: 20px;">
								<p>Client Name</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="image-small-icon col-md-3">
										<img class="img-circl-large" src="{{asset('/images/4.png')}}" tilte="">
									</div>
									<div class="col-md-4">
										<h4 >Client Name</h4>
										<p>{{$userAssistance[0]->name}}</p>
										<h4>Phone Number</h4>

										<?php if(empty($userAssistance[0]->phoneNo)){?><p>No phone number</p> <?php	}else{ ?>
											<p>{{$userAssistance[0]->phoneNo}}</p> 
										<?php } ?>
									</div>
									<div class="col-md-4">
										<h4>Date of Birth</h4>
										<?php if(empty($userAssistance[0]->dob)){?><p>No Dob</p> <?php	}else{ ?>
											<p>{{$userAssistance[0]->dob}}</p> 
										<?php } ?>

										<h4>Email</h4>
										<p>{{$userAssistance[0]->email}}</p>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="image-small-icon col-md-3">
										
										<?php if($userAssistance[0]->assistance != null || $userAssistance[0]->assistance != ''){
											?>
											<img class="img-circl" src="{{$userAssistance[0]->assistance->avatar}}" tilte="">
											<p style="text-align: center;margin-top: 0px;font-weight: bold;">{{$userAssistance[0]->assistance->name}}</p>
										<?php } else { ?>	
											<img class="img-circl" src="{{asset('/images/4.png')}}" tilte="">
											<p style="text-align: center;margin-top: 0px;font-weight: bold;">No avatar</p>
										<?php } ?>

									</div>

									<?php
									use Illuminate\Database\Eloquent\Collection;
									?>
									<?php
									$collection = collect($services);
									$newserviceCount = $collection->where('isnew', '1')->count();
									$pendingServiceCount = $collection->where('status', 'open')->count();
									$completedServiceCount = $collection->where('status', 'closed')->count();
									?>
									<div class="col-md-9" style="font-size:medium;line-height:25px">   <p>Service requests - {{$collection->count()}}</p>
										<p>recent service requests - {{$newserviceCount}}</p>
										<p>Pending service requests - {{$pendingServiceCount}}</p>
										<p>Completed service requests - {{$completedServiceCount}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-7">
							<div class="panel panel-default">
								<div class="panel-body" style="display:block">
									<div class="col-md-12">
										<h4>Clients conversation with assistant :</h4>
									</div>
									<div class="col-md-12">
										<table class="table">
											@foreach($services as $service)
											<tr>
												<td><h5>{{$service->subject}}</h5><p>Hey you can tell me the date of join</p></td>
												<td>{{ Carbon\Carbon::parse($service->updated_at)->format('d/m/Y')}}</td>
											</tr>
											@endforeach()

										</table>
									</div>	
								</div>
							</div>
						</div>

						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-body">

								</div>
							</div>
						</div>
					</div>	

				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->

	</div> <!-- container -->
</div> <!-- content -->

@stop