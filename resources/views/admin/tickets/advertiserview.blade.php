@extends('admin.layouts.admin')

@section('content')

<div class="content">
    <div class="container">

	<div class="row">
	    <div class="col-sm-12">
		<div class="panel panel-default">
		    <div class="panel-heading">

			<div class="row">
			    <div class="create_user col-md-1" style="font-weight: 800;font-size: 20px;">
				<p>Tickets</p>
			    </div>
			    <div class="search-button col-md-8">
				<input type="text" name="search" value="" placeholder="Search Title" />

			    </div>
			    
			    <input type="radio" name="status" id="allstatus" value="all" {{ ($all_view == 1)? 'checked="true"' : '' }}  ><label for="allstatus" onclick="fetchrecords(this.value)">All</label>
			    <input type="radio" name="status" value="open" id="pendingstatus" {{ ($pending_view == 1)? 'checked="true"' : '' }}><label for="pendingstatus" onclick="fetchrecords(this.value)" >Pending</label>
			</div>
		    </div>
		    <div class="panel-body">
			<table class="table table-hover" style="margin-left: 50px;">

			    <thead>

				<tr>
				    <td>Sr No.</td>
				    <td>Status</td>
				    <td>Title</td>
				    <td>Date Created</td>
				    <td>Last Reply</td>
				    <td>Action</td>
				</tr>
			    </thead>
			    <tbody>
				@php
				    $i = 1
				@endphp
				@if($advertiserTickets->isEmpty())
				<tr>
				    <td colspan="5" align="center">There are no Advertiser Tickets.</td>
				</tr>
				@else
				@foreach($advertiserTickets as $tickets)
				<tr>

				    <td>{{ $i }}</td>
				    <td>{{ $tickets['status'] }}</td>
				    <td>{{ $tickets['ticketName'] }}</td>
				    <td>{{ $tickets['created_at']->toDateString() }}</td>
				    <td>
					
					{{ $tickets->ticket_chat->last()->getLastRepliedUser->type ?? '' }}
				    </td>

				    <td>
					<a href="{{ route('tickets.advertiserTicketview', $tickets['id']) }}">
					    View

					</a>
				    </td>

				</tr>
				@php
				    $i++
				@endphp
				@endforeach
				@endif
			    </tbody>
			</table>
		    </div>

		</div>
	    </div> <!-- end col -->
	</div> <!-- end row -->

    </div> <!-- container -->
</div> <!-- content -->
@stop


@section('js')

<script>
    $(document).ready(function () {

        $(document).on('click', '#pendingstatus', function () {
	    document.location.href = "{!! route('tickets.advertiser.pending') !!}";
        })

        $(document).on('click', '#allstatus', function () {
            document.location.href = "{!! route('tickets.advertiser') !!}";
        })
    });
</script>

@stop