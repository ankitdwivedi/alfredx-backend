@extends('admin.layouts.admin')

@section('content')
<div class="container">
    <div class="col-md-6" style="text-align: left; float: left;">
	<span style="font-size: 32px;"><b>{{$userTickets->ticketName}}</b></span>
    </div>
    @if($userTickets->status == "open")
    <div class="col-md-6" style="text-align: right;">
	{!! Form::open(['route' => 'tickets.closeClientTicket']) !!}


	<input type="hidden" name="status" value="closed">
	<input type="hidden" name="ticket_id" value="{{$ticketId}}">

	<button type="submit" class="btn btn-default" style="vertical-align: top">Close Ticket</button>


	{!! Form::close() !!}
    </div>
    @endif
    @foreach($userTicketChat as $chats)


    @if($userTickets->user_id == $chats['sender_id']) 
    <div class="col-md-12" style="text-align: left; float: left;">
	{{ $chats['message'] }}
    </div>
    @else
    <div class="col-md-12" style="text-align: right; float: left;">
	{{ $chats['message'] }}
    </div>
    @endif
    @endforeach

    {!! Form::open(['route' => 'tickets.storeMessageclient']) !!}
    <div class="col-md-12" style="margin-top: 10px;">
	{!! Form::textarea('message', null, ['cols' => 90,'rows' => 2, 'required' => true]) !!}
	{!! $errors->first('message', '<div class="text-danger">:message</div>') !!}
	<input type="hidden" name="sender_id" value="{{Auth::user()->id}}">
	<input type="hidden" name="reciever_id" value="{{$userTickets->user_id}}">
	<input type="hidden" name="ticket_id" value="{{$ticketId}}">

	<button class="btn btn-default" style="vertical-align: top">Send</button>
    </div>

    {!! Form::close() !!}

</div>

@stop
