@extends('admin.layouts.admin')

@section('content')
<!-- Start content -->


<div class="content">

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Registered Users</h4>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body dashboard">
									<div class="col-md-4" style="border-right: 1px solid #e0e2e2; padding-left: 30px;" >
										<h4 style="margin:auto;">Total Registed Clients :</h4>
										<h3 class="red-col">{{$totalUsers}}</h3>
										<h4 style="margin:auto;">Total Clients in last 7 days :</h4>
										<h3 class="red-col">{{$last7days}}</h3>
									</div>
									<div class="col-md-8" style="padding-left: 40px;">
										<h4 style="margin:auto;">Top 5 Clients :</h4>

										<table class="table" style="margin-top: 10px;">
											@foreach($top5lists as $top5list)
											<tr>
												<td>{{$top5list->name}}</td>
												<td>{{$top5list->email}}</td>
												<td>{{$top5list->phoneNo}}</td>
												<td><a class="red-col">view</a></td>
											</tr>
											@endforeach()
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row internal-row">
						<div class="search-button col-md-8">
							<input type="text" name="search" value="" placeholder="search by text" />
						</div>
						<div class="create_user col-md-4" style="font-weight: 800;text-decoration: underline;text-align: right;font-size: 16px;text-underline-position: under;">
							<a class="red-col" href="#" title="">Create New Assistance</a>
						</div>
					</div>
					<br>
					<div class="row">
						<?php
						use Illuminate\Database\Eloquent\Collection;
						?>
						@foreach($assistants as $assistant)
						<?php
						$collection = collect($assistant->service);
						$newserviceCount = $collection->where('isnew', '1')->count();
						$pendingServiceCount = $collection->where('status', 'open')->count();
						$completedServiceCount = $collection->where('status', 'closed')->count();
						?>

						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="image-small-icon col-md-3">
										<img class="img-circl" src="{{asset('/images/4.png')}}" tilte="">
										<p style="text-align: center;margin-top: 0px;font-weight: bold;">{{ucfirst($assistant->name)}}</p>
									</div>
									<div class="col-md-9">
										<p>New service request - {{$newserviceCount}}</p>
										<p>Pending service request - {{$pendingServiceCount}}</p>
										<p>Completed service request - {{$completedServiceCount}}</p>
										<div style="text-align: right;margin-top: 5px;"><a class="red-col">View Details</a> </div>
									</div>
								</div>
							</div>
						</div>
						@endforeach()
					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->

	</div> <!-- container -->
</div> <!-- content -->


@stop