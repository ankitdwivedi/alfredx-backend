<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Alfredx</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/app.css') }}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/admin.css') }}" />
</head>
<body>
  <section class="section-1">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-12 col-12  text-center login-panel">
          <div class="form_layout">
            <div class="row">
              <div class="col-md-12 brand-logo">
                <img src="{{ asset('/images/Logo@2x.png') }}" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center login-title">
                <b>Forgot Password</b>
              </div>
            </div>
             <br>
            <div class="col-md-12 text-center">
              <span style="font-size: 14px;color: #6b6b6b;font-weight: 800;">Please enter your registered email</span>
            </div>
            <form>
              {!! csrf_field() !!}
              <input type="email" name="field2" placeholder="E-mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" />
              <div class="form-group text-muted text-right">
              </div>
              <div class="form-group">
                <button  type="submit" class="submit-btn">Submit</button>
              </div>
            </form>
          </div>
        </div>  
      </div>
    </div>
  </div>
</section>
</body>
</html>