@extends('admin.layouts.admin')

@section('content')
<!-- Start content -->
<div class="content">
	<div class="container">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="create_user col-md-6" style="font-weight: 800;font-size: 20px;">
								<p>Assistant Name</p>
							</div>
							
							<div class="create_user col-md-6" style="font-weight: 800;text-decoration: underline;text-align: right;font-size: 16px;text-underline-position: under;padding-right:30px;">
								<a class="red-col" id="sa-warning" href="#" title="">Delete Assistant</a>
								{{csrf_field()}}
							</div>

						</div>
					</div>

					<div class="row">
						
						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="image-small-icon col-md-3">
										
										<img class="img-circl" alt="{{asset('/images/4.png')}}" src="{{$userAssistance->avatar}}" tilte="">
										<p style="text-align: center;margin-top: 0px;font-weight: bold;">{{$userAssistance->name}}</p>
									</div>

									<?php
									use Illuminate\Database\Eloquent\Collection;
									?>
									<?php
									$collection = collect($userAssistance->service);
									$newserviceCount = $collection->where('isnew', '1')->count();
									$pendingServiceCount = $collection->where('status', 'open')->count();
									$completedServiceCount = $collection->where('status', 'closed')->count();
									?>
									<div class="col-md-9" style="font-size:medium;line-height:25px">   <p>Service requests - {{$collection->count()}}</p>
										<p>recent service requests - {{$newserviceCount}}</p>
										<p>Pending service requests - {{$pendingServiceCount}}</p>
										<p>Completed service requests - {{$completedServiceCount}}</p>
									</div>
								</div>
							</div>
						</div> 
					</div>


					<div class="row chat-list">
						<div class="col-md-7">
							<div class="panel panel-default">
								<div class="panel-body" style="display:block">
									<div class="col-md-12">
										<h4>Conversation with client :</h4>
									</div>


									@foreach($userAssistance->service as $service)

									<div class="col-md-12 chat-box" style="padding: 4px;">
										<div class="col-md-2" style="text-align: center;">
											<img class="img-circl-tiny" src="{{asset('/images/4.png')}}" tilte="">
										</div>
										<div class="col-md-8" style="font-size:medium;line-height:25px">
											<h5 style="font-size: 15px;">{{$service->subject}}</h5>
											<p>Hey you can tell me the date of juorny</p>
										</div>
										<div class="col-md-2" style="font-size:medium;line-height:25px">
											<p>{{ Carbon\Carbon::parse($service->updated_at)->format('d/m/Y')}}</p>
										</div>
									</div>	

									@endforeach()


								</div>
							</div>
						</div>

						<div class="col-md-5">
							<div class="panel panel-default">
								<div class="panel-body">

								</div>
							</div>
						</div>



					</div>	


				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->

	</div> <!-- container -->
</div> <!-- content -->

@stop