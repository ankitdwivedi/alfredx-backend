@extends('admin.layouts.admin')

@section('content')
<!-- Start content -->


<div class="content">

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
			</div>
		</div>
		<!-- Page-Title -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					
					<div style="padding:20px;">
						<div class="row">
							<div class="create_user col-md-8">
								<h4 class="panel-title">Assistants</h4>
							</div>
							<div class="create_user col-md-4" style="font-weight: 800;text-decoration: underline;text-align: right;font-size: 16px;text-underline-position: under;">
								<a class="red-col" href="#" title="">Create New Assistant</a>
							</div>
						</div>
					</div>


					<br>
					<div class="row">
						<?php
						use Illuminate\Database\Eloquent\Collection;
						?>
						@foreach($assistants as $assistant)
						<?php
						$collection = collect($assistant->service);
						$newserviceCount = $collection->where('isnew', '1')->count();
						$pendingServiceCount = $collection->where('status', 'open')->count();
						$completedServiceCount = $collection->where('status', 'closed')->count();
						?>

						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="image-small-icon col-md-3">
										<img class="img-circl" src="{{$assistant->avatar}}" tilte="">
										<p style="text-align: center;margin-top: 0px;font-weight: bold;">{{ucfirst($assistant->name)}}</p>
									</div>
									<div class="col-md-9">
										<p>New service request - {{$newserviceCount}}</p>
										<p>Pending service request - {{$pendingServiceCount}}</p>
										<p>Completed service request - {{$completedServiceCount}}</p>
										<div style="text-align: right;margin-top: 5px;"><a href="{{url('admin/assistant',[$assistant->id])}}" class="red-col">View Details</a> </div>
									</div>
								</div>
							</div>
						</div>

						@endforeach()

					</div>
				</div>
			</div> <!-- end col -->
		</div> <!-- end row -->

	</div> <!-- container -->
</div> <!-- content -->


@stop