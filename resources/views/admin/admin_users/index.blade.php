@extends('admin.layouts.admin')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
<!-- Start content -->
<div class="content">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <div class="row">
                            <div class="create_user col-md-4" style="font-weight: 800;font-size: 20px;">
                                <p>Admin Users <label style="color:red;">({{ count($paginator) }})</label></p>
                            </div>
                            <div class="search-button col-md-4">
                            </div>
                            <div class="create_user col-md-2" style="font-weight: 800;text-decoration: underline;text-align: right;font-size: 16px;text-underline-position: under;">
                                <a class="red-col" href="#exampleModal3changepassword" id="modal_link_change_password" rel="modal:open" style="display:none;" title="">changepassword</a>
				
				<a class="red-col" href="#exampleModal3admin" id="modal_link_create" rel="modal:open" style="display:block;" title="">Create New</a>
                               
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
				    <th>Email</th>
                                    <th>Created Date</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($paginator as $admin_user)
                                <tr>									
                                    
                                    <td class="">{{$admin_user->email}}</td>
                                    <td class="">{{$admin_user->created_at}}</td>
                                    <td class="text-center">
                                        
                                        
					                                        <a class="red-col btn btn-danger" href="javascript:void(0);" id="delete_user" data-id="{{ $admin_user->id }}" >Delete User</a>
                                        
										<a class="btn btn-primary" role="button" href="javascript:void(0);" id="change_password" data-id="{{ $admin_user->id }}" data-email="{{ $admin_user->email }}" data-name="{{ $admin_user->name }}" style="background-color: grey !important">Change Password</a>
									
                                        <!--<a href="{{url('admin/admin_users',[$admin_user->id])}}" class="btn btn-info">view</a>-->
                                    </td>
                                </tr>
                                @endforeach()
                            </tbody>
                        </table>
                    </div>
                    <div style="float:right">{{$paginator}}</div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container -->
</div> <!-- content -->

<div class="modal" id="exampleModal3admin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="max-width: 650px !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form method="POST" action="{{url('admin/admin_users')}}" class="form-horizontal add-form ajax" id="admin_user_form_admin"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="type" value="admin" />
                <input type="hidden" name="id" id="admin_id" value="" />
                <div class="form-group form-inline">
                    <label for="name" class="inline-lab">Name:</label>
                    <input type="text" class="form-control " id="name" name="name" required />
                </div>
                <div class="form-group form-inline">
                    <label for="email" class="inline-lab">Email:</label>
                    <input type="email" class="form-control required email" id="email" name="email" required />
                </div>
                <div class="form-group form-inline">
                    <label for="password" class="inline-lab">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" required/>
                </div>
                <div class="form-group form-inline">
                    <label for="retype_password" class="inline-lab">Retype Password:</label>
                    <input type="password" class="form-control" id="retype_password" name="retype_password" required />
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Close</button>
		    
                </div>
            </form>
        </div>
    </div>
</div> 

<div class="modal" id="exampleModal3changepassword" tabindex="-1" role="dialog" style="max-width: 650px !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <form method="POST" action="{{url('admin/admin_users')}}" class="form-horizontal add-form ajax" id="admin_user_form_password"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="type" value="admin" />
                <input type="hidden" name="id" id="admin_id" value="" />
               
                <div class="form-group form-inline">
                    <label for="password" class="inline-lab">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" required/>
                </div>
                <div class="form-group form-inline">
                    <label for="retype_password" class="inline-lab">Retype Password:</label>
                    <input type="password" class="form-control" id="retype_password" name="retype_password" required />
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" id="changepasswordclose" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Close</button>
                </div>
            </form>
        </div>
    </div>
</div> 
@stop

@section('css')
<style>
    .modal a.close-modal{
	top: -2px !important;
	right: 0px !important;
    } 
</style>
@endsection

@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    var password = document.getElementById("password")
            , retype_password = document.getElementById("retype_password");

    function validatePassword() {
        if (password.value != retype_password.value) {
            retype_password.setCustomValidity("Passwords Don't Match");
        } else {
            retype_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
//confirm_password.onkeyup = validatePassword;
    retype_password.onkeyup = validatePassword;


    /**
     * Change Password
     * */

//$(document).ready(function(){
    $(document).on('click', '#change_password', function () {
        id = $(this).data('id');
        name = $(this).data('name');
        email = $(this).data('email');
        var url = "{{ url('admin/admin_users/change_password') }}";
        $("#admin_user_form_password").attr('action', url);
        $("#admin_id").val(id);
      
        $("#modal_link_change_password").trigger('click');
    });

    $(document).on('click', '#create_new_admin', function () {
        var url = "{{ url('admin/admin_users') }}";
        $("#admin_user_form_admin").attr('action', url);
        $("#admin_id").val('');
	
        $("#modal_link_create").trigger('click');
    });

    $(document).on('click', '#delete_user', function () {
        if (!confirm("Are you sure to delete this admin user?")) {
            return false;
        } else {
            id = $(this).data('id');
            //alert("{{ url('admin/admin_users') }}/" +  id);
            $.ajax({
                url: "{{ url('admin/admin_users') }}/" + id,
                type: 'json',
		method: 'delete',
                data: { '_token':'{{ csrf_token()}}' },
                success: function (response) {
		    if(response.status == "success"){
		     document.location.href="{{ url('admin/admin_users') }}";
		}
                },
            })
        }
    });
    
     
     
});

</script>
@endsection