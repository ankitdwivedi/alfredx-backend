@extends('admin.layouts.admin')

@section('content')
<!-- Start content -->
<div class="content">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="create_user col-md-6" style="font-weight: 800;font-size: 20px;">
                                <p>Admin User Name</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="image-small-icon col-md-3">
                                        <img class="img-circl-large" src="{{asset('/images/4.png')}}" tilte="">
                                    </div>
                                    <div class="col-md-4">
                                        <h4>Email</h4>
                                        <p>{{$admin_users[0]->email}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container -->
</div> <!-- content -->

@stop