<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Alfredx</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/app.css') }}" />
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/admin.css') }}" />
</head>
<body>
  <section class="section-1">
    <div class="container">
      <div class="row">

        <div class="col-md-5 col-sm-12 col-5  text-center login-panel">
          <div class="form_layout">
            <div class="row">
              <div class="col-md-12 brand-logo">
                <img src="{{ asset('/images/Logo@2x.png') }}" alt="">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center login-title">
                <b>Sign In</b>
              </div>
            </div>
            <form action="{{ route('aattemptLogin') }}" method="POST">
              {!! csrf_field() !!}
              <input type="email" name="email" placeholder="E-mail" value="{{old('email')}}"/>
              @if($errors->has('email'))
              <p class="field-errors">{{$errors->first('email')}}</p>
              @endif
              <input type="password" name=
              "password" placeholder="Password" />
              @if($errors->has('password'))
              <p class="field-errors">{{$errors->first('password')}}</p>
              @endif
              <div class="form-group text-muted text-right">
                <span><a href="{{route('aforgotPassword')}}"><b>Forgot Password?</b></a></span>
              </div>
              <div class="form-group">
                <button  type="submit" class="submit-btn">Sign In</button>
              </div>
            </form>
          </div>
        </div>  
        <div class="col-md-5 col-sm-12 col-5  text-center login-panel ">
         <div>
          <img src="{{ asset('/images/abklogo.png') }}" alt="">
        </div>
      </div>


    </div>
  </div>
</div>
</section>

</body>
</html>