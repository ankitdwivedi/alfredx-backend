@extends('admin.layouts.admin')

@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
<table class="table table-hover" style="margin-left: 100px;">
    <thead>
         <tr style="float: right;">
            <td>
                <a href="{{ route('cms.create') }}" class="btn btn-primary">Create New Page</a>
            </td>
        </tr>
    </thead>
    <thead>
       
        <tr>
            <td>Title</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>
    </thead>
    <tbody>

        @if($cmspages->isEmpty())
        <tr>
            <td colspan="5" align="center">There are no pages.</td>
        </tr>
        @else
        @foreach($cmspages as $page)
        <tr>


            <td>{{ $page['title'] }}</td>

            <td>
                <a href="{{ route('cms.edit', $page['id']) }}">
                    <i class="fa fa-pencil" aria-hidden="true"></i>

                </a>
            </td>
            <td>
<!--		<form method="delete" action="{{ route('cms.destroy', $page['id']) }}">
		    <button type="submit" >Delete</button>-->
                <a href="javascript:void(0);" class='delete' data-route="{{ route('cms.destroy', $page['id']) }}">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>

                </a>
		<!--</form>-->
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>

@stop

@section('js')
<script>
    
    $(document).ready(function(){
	$(document).on('click', '.delete', function() {
    var route = $(this).data('route');
//    var route1 = $(this).attr('data-route');
//    alert(route);
//    alert(route1);
    if(!confirm('Are you sure you want to delete this page?')) {
	return false;
    } else {
//	console.log(route);
	$.ajax({
	    url: route,
	    type: 'json',
	    method: 'DELETE',
	    data: {'_token': "{{ csrf_token() }}"},
	    success: function(res){
		
		if(res.status == "success"){
		     document.location.href="{!! route('cms.index') !!}";
		}
	    }

	});
    }
})
    })

</script>
@endsection