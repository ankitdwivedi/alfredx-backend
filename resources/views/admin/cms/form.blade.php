@extends('admin.layouts.admin')
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>

<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
  <script>
  tinymce.init({
    selector: 'textarea'
  });
  </script>

@section('title', $cmspage->exists ? 'Editing '.$cmspage->title : 'Create New Page')

@section('content')




    {!! Form::model($cmspage, [
        'method' => $cmspage->exists ? 'put' : 'post',
        'route' => $cmspage->exists ? ['cms.update', $cmspage->id] : ['cms.store']
    ]) !!}

    <div class="form-group" style="margin-left: 40px;">
        {!! Form::label('title') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
	{!! $errors->first('title', '<div class="text-danger">:message</div>') !!}
    </div>
    

    <div class="form-group" style="margin-left: 40px;">
        {!! Form::label('content') !!}
        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
	{!! $errors->first('content', '<div class="text-danger">:message</div>') !!}
    </div>
  
    <div class="form-group" style="margin-left: 40px;">
        {!! Form::submit($cmspage->exists ? 'Save Page' : 'Create New Page', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}>
    
@endsection
@section('css')
<style>
	.mce-notification-inner {display:none!important;}
	</style>
@endsection
