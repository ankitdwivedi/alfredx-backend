<?php

Auth::routes(['verify' => true]);


Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::get('/verify', function() {return view('auth.verify');} );
Route::post('/login', 'Auth\LoginController@attemptLogin')->name('attemptLogin');
Route::post('/password', 'HomeController@changePassword')->name('changePassword');
Route::post('/update/Profile', 'HomeController@updateProfile')->name('updateProfile');

Route::get('/forgot', function () { return view('ForgotPassword'); })->name('forgotPassword');
Route::get('/details', function () { return view('Details'); })->name('details');
Route::get('/register', function () { return view('auth.register'); })->name('signUp')->middleware('guest');
Route::get('/home', function () { return view('backend.dashboard'); })->name('dashboard')->middleware('auth');

Route::post('/register', 'Auth\RegisterController@create' )->name('saveUser');

Route::get('/campaign/create', function(){return view('backend.campaign.createNewCampaign'); } )->name('campaign.create');
Route::get('/campaign/ad/create', function(){return view('backend.campaign.newCampaignAd'); } )->name('campaignAd.create');
Route::get('/campaign/target/create', function(){return view('backend.campaign.newCampaignTarget'); } )->name('campaignTarget.create');
Route::get('/campaign/ready', function(){return view('backend.campaign.newCampaignReady'); } )->name('campaignReady.create');

Route::get('/subscriptions', function(){return view('backend.subscriptions'); } )->name('subscriptions');
Route::get('/about', function(){return view('backend.aboutus'); } )->name('aboutus');
Route::get('/about', function(){return view('backend.aboutus'); } )->name('aboutus');
Route::get('/tickets/create', function(){return view('backend.tickets.create'); } )->name('tickets.create');
Route::get('/reports', function(){return view('backend.reports'); } )->name('reports');
Route::get('/support-tickets', function(){return view('backend.supportTickets'); } )->name('supportTickets');
Route::get('/support-tickets-view-open', function(){return view('backend.supportTicketsViewOpen'); } )->name('supportTicketsViewOpen');

/*ADMIN*/
Route::group(['prefix' => 'admin','namespace'=>'Admin'], function () {
//	Route::get('/login', function(){return view('admin.alogin'); })->middleware('auth:web');
	Route::get('/login', 'ALoginController@index');//->middleware('auth:web');
	Route::post('/login', 'ALoginController@attemptLogin')->name('aattemptLogin');
	Route::get('/forgotPassword', function(){return view('admin.aforgotPassword'); })->name('aforgotPassword');
	/*CUSTOMIZE THE ROOT ELEMENTS*/	
	Route::group(['middleware'=>'auth'], function () {
		Route::get('/logout','ALoginController@logout')->name('alogout');
		Route::get('/dashboard', 'ADashboardController@index')->name('adashboard');
		Route::resource('clients', 'ClientsController');	
		Route::resource('assistant', 'AssistantsController');	
		Route::post('admin_users/change_password', 'AdminController@change_password')->name('admin_users.change_password');
		Route::resource('admin_users', 'AdminController');
                Route::resource('cms', 'CmsController');
		Route::get('/tickets/clients','TicketsController@index')->name('tickets.clients');
		Route::get('/tickets/clients/pending','TicketsController@pendingTickets')->name('tickets.clients.pending');
		Route::get('/tickets/clientTicketview/{id}','TicketsController@clientTicketview')->name('tickets.clientTicketview');
		Route::post('/tickets/storeMessageclient/','TicketsController@storeMessageclient')->name('tickets.storeMessageclient');
		Route::post('/tickets/closeClientTicket/','TicketsController@closeClientTicket')->name('tickets.closeClientTicket');
		Route::get('/tickets/advertiser','TicketsController@advertiser')->name('tickets.advertiser');
		
		Route::get('/tickets/advertiser/{id}','TicketsController@advertiserTicketview')->name('tickets.advertiserTicketview');
		
		Route::get('/tickets/advertiser/pending','TicketsController@pendingTicketsAdvertiser')->name('tickets.advertiser.pending');
		
		Route::post('/tickets/closeAdvertiseTicket/','TicketsController@closeAdvertiseTicket')->name('tickets.closeAdvertiseTicket');
		
		Route::post('/tickets/storeMessageadvertise/','TicketsController@storeMessageadvertise')->name('tickets.storeMessageadvertise');
		
	});
});

Route::group(['prefix' => 'campaign'], function() {
	Route::post('/campaign/create', ['as' => 'campaign.create', 'uses' => 'Web/AdvertiseController@create']);
	Route::post('/campaign/store', ['as' => 'campaign.store', 'uses' => 'Web/AdvertiseController@store']);
});


