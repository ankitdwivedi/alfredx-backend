<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1','namespace'=>'Api'], function () {
	Route::post('login', 'AuthController@login');
	Route::post('signup', 'AuthController@signup');
	Route::post('socialLogin', 'AuthController@socialLogin');

	Route::post('profileupdate/{id}','AuthController@profileUpdate');
	Route::post('forgotpassword', 'AuthController@sendResetLinkEmail');
	
	Route::get('getAssistances', 'ExternalController@getAssistance');

	Route::group(['middleware' => 'auth:api'], function() {
		Route::get('logout', 'AuthController@logout');
		Route::get('user', 'AuthController@user');
		Route::post('resetPassword/{id}','AuthController@resetPassword');
		/*User module api*/ 
		Route::post('createService', 'ServiceController@createService');
		Route::get('getService', 'ServiceController@getService');	
		Route::get('getAssistanceService/{assistance_id}', 'ServiceController@getAssistanceService');
		
		Route::get('serviceDashboard', 'ServiceController@serviceDashboard');
		Route::post('updateServiceStatus/{id}', 'ServiceController@updateServiceStatus');
		Route::resource('drives', 'DrivesController');
		Route::get('ticket/list', 'TicketsController@index');
		/* Login User Profile Update - dt: 19th December 2018 */
        Route::post('updateMyProfile', 'AuthController@UpdateMyProfile')->name('api.update_my_profile');
	Route::post('update-assistant', 'ExternalController@updateAssistant');
		/*advertise module api*/
	Route::post('changePassword','AuthController@changePassword');
	Route::post('reopenTicket','TicketsController@reopenTicket');
	Route::get('getallassitant','ServiceController@getAllAssitant');
	});

	Route::resource('reminder','RemindersController');
	Route::post('updateStatus','RemindersController@updateStatus');
	


	Route::resource('advertise', 'AdvertisesController');

	Route::resource('ticket', 'TicketsController');
	Route::post('postMsgTicket', 'TicketsController@postMsgTicket');
	Route::get('getMsgTicket/{id}', 'TicketsController@getMsgTicket');


});

