<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverties', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            $table->string('compaignName')->nullable();
            $table->string('accountName')->nullable();
            $table->string('type')->nullable();
            $table->text('advertUrl')->nullable();
            $table->integer('impression')->nullable();
            $table->integer('click')->nullable();
            $table->integer('converstions')->nullable();
            $table->text('description')->nullable();
            $table->string('pkspent')->nullable();
            $table->enum('status',['publish','unpublish'])->default('unpublish');
            $table->enum('targetAudience',['male','female','near'])->default('near');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverties');
    }
}
