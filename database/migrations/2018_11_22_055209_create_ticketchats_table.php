<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketchats', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ticket_id');
            $table->integer('sender_id');
            $table->integer('reciever_id');
            $table->text('message');
            $table->enum('ticketFrom',['0','1'])->default('0')->comment("0-user side, 1-admin side");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketchats');
    }
}
