<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserAssistants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_assistants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userId');
            $table->string('assistantId');
            $table->enum('isCurrent',['0','1'])->default('0')->comment("0-Not Current Assistant, 1-Current Assistant");
            $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_assistants');
    }
}
