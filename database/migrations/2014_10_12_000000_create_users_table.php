<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('dob')->nullable();
            $table->string('phoneNo')->nullable();
            $table->string('contactPerson')->nullable();
            $table->string('contactDetail')->nullable();
            $table->integer('assistance_id')->nullable();
            $table->enum('gendor',['male','female'])->nullable();
            
            $table->string('socialId')->nullable();
            $table->enum('login_type',['0','1'])->default('0')->nullable()->comment("0-normal user, 1-social login");

            $table->enum('type', ['user','advert','admin'])->nullable();    
            $table->enum('platform',['ios','android'])->nullable();
            $table->string('deviceToken')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            
            $table->integer('subcription_id')->nullable();
            $table->enum('targetAudience',['male','female','near'])->default('near');            

            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
