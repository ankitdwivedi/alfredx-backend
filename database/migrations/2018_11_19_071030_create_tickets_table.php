<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            $table->string('ticketName');
            $table->string('description');
            $table->enum('priority',['0','1','2'])->default('0')->comment('0-low,1-medium,2-high');
            $table->enum('ticketFrom',['user','advert'])->default('user');
            $table->enum('status',['open','closed','reopen'])->default('open');
                        
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
